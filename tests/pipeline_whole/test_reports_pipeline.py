from os import remove
from os.path import isfile, join, getsize
from tempfile import gettempdir

import pytest

from reports_generation import ReportsPipeline


@pytest.fixture(scope='function')
def pipeline():
    return ReportsPipeline(
        genotypes_vcf=pytest.helpers.file('genotypes.vcf'),
        outdir=gettempdir(),
        variants_json=pytest.helpers.file('variants.json'),
        genes_json=pytest.helpers.file('genes.json'),
        vep_tsv=pytest.helpers.file('vep.tsv'),
        min_reportable_category='DRUG',
        min_odds_ratio=1,
        max_frequency=1,
        phenos_regex_list=['.?'],
        phenos_regex_file=pytest.helpers.file('phenos_regex.list'),
    )


def test_initialization(pipeline):
    assert pipeline.phenos_regex_list == ['.?', '.*', 'pattern-1', 'pattern-2']
    assert pipeline.max_frequency == 1
    assert pipeline.min_odds_ratio == 1
    assert pipeline.min_reportable_category == 'DRUG'
    assert not pipeline.variant_annotations.empty
    assert not pipeline.gene_annotations.empty
    assert not pipeline.vep_annotations.empty


def test_read_phenos_regex_file(pipeline):
    pipeline.phenos_regex_list.clear()
    assert len(pipeline.phenos_regex_list) == 0

    pipeline.phenos_regex_file = pytest.helpers.file('phenos_regex.list')
    pipeline._read_phenos_regex_file()
    assert 'pattern-1' in pipeline.phenos_regex_list
    assert 'pattern-2' in pipeline.phenos_regex_list


def test_read_annotations_files(pipeline):
    pipeline.variant_annotations = None
    pipeline.gene_annotations = None
    pipeline.vep_annotations = None
    pipeline._read_annotation_files()

    n_variants = 2  # These come from genotypes.vcf
    n_columns = 24  # Total number of columns expected
    assert pipeline.variant_annotations.shape == (n_variants, n_columns)
    n_genes = 3  # These are in the genotypes.vcf
    n_columns = 3
    assert pipeline.gene_annotations.shape == (n_genes, n_columns)
    assert len(pipeline.vep_annotations) == 6


def test_run_for_all_samples(pipeline):
    pipeline.run()
    check_pipeline_results(pipeline)


def test_run_for_single_sample(pipeline):
    sample_id = 'HG00096'
    pipeline.run(samples=sample_id)
    check_pipeline_results(pipeline, sample_ids=[sample_id])


def test_run_for_some_samples(pipeline):
    sample_ids = ['HG00096', 'HG00097']
    pipeline.run(samples=sample_ids)
    check_pipeline_results(pipeline, sample_ids=sample_ids)


def check_pipeline_results(pipe, sample_ids=[]):
    for sample_id, sample_data in pipe.sample_results.items():
        assert not sample_data['variants'].empty

        expected_report_path = join(
            gettempdir(),
            pipe.sample_results[sample_id]['report_data_json_path']
        )
        assert isfile(expected_report_path)
        assert getsize(expected_report_path)

        # Cleanup!
        remove(expected_report_path)


# These three methods could be separately tested, although they
# are tested when the whole pipe is run.
@pytest.mark.skip(reason='Test not implemented')
def test_sample_pipeline(pipeline):
    pass


@pytest.mark.skip(reason='Test not implemented')
def test_annotate_sample_genotypes(pipeline):
    pass


@pytest.mark.skip(reason='Test not implemented')
def test_select_sample_reportable_genotypes(pipeline):
    pass
