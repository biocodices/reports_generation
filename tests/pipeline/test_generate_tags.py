from reports_generation.pipeline import generate_tags


def test_generate_tags():
    variant = {
        'all_clinvar_variations': [{}],
        'reportable_clinvar_variations': [{'genomic_allele': 'A'}],
        'omim_entries': [{}],
        'reportable_gwas_associations': [],
        'reportable_snpeff_annotations': [],
        'reportable_vep_annotations': [],
        'vcf_ref': 'A',
        'vcf_alt': ['T'],
        'vcf_filter': 'Filter-1',
        'FT': None,
        'risk_alleles': ['A'],
        'present_risk_alleles': ['A'],
        'has_risk_allele': True,
        'is_REV': True,
        'gmaf': 0.01,
    }

    tags = generate_tags(variant)
    assert tags == [
        'clinvar variations',
        'omim entries',
        'filters not passed',
        'is REV',
        'REF risk allele',
    ]

    variant.update({
        'risk_alleles': [],
        'present_risk_alleles': [],
        'gmaf': 0.25,
        'vcf_filter': 'PASS',
        'FT': 'Filter-2',
    })
    tags = generate_tags(variant)
    assert 'uncertain risk allele' in tags
    assert 'high frequency alleles' in tags
    assert 'filters not passed' in tags
    assert 'sample might not have the risk allele' in tags

    variant.update({'reportable_vep_annotations': [{'genomic_allele': 'A'}]})
    tags = generate_tags(variant)
    assert 'vep annotations' in tags

    variant.update({'reportable_snpeff_annotations': [{'genomic_allele': 'A'}]})
    tags = generate_tags(variant)
    assert 'snpeff annotations' in tags

    variant['present_risk_alleles'] = ['G']
    tags = generate_tags(variant)
    assert 'risk allele not seen in VCF' in tags

    variant['reportable_clinvar_variations'] = [{'genomic_allele': None}] * 3
    tags = generate_tags(variant)
    assert tags.count('entries with uncertain allele') == 1

    variant['all_clinvar_variations'].clear()
    tags = generate_tags(variant)
    assert 'not in clinvar' in tags

    del(variant['FT'])
    generate_tags(variant)  # Shouldn't break with missing FT key

