import pandas as pd

from reports_generation.pipeline import (
    merge_genotypes_and_annotations,
)

from reports_generation.pipeline.merge_genotypes_and_annotations import (
    _id_or_location_from_VCF,
)


def test_id_or_location():
    variant = pd.Series({'vcf_id': 'rs1'})
    assert _id_or_location_from_VCF(variant) == 'rs1'

    variant = pd.Series({'vcf_id': '.', 'vcf_chrom': '1', 'vcf_pos': 1000.0})
    assert _id_or_location_from_VCF(variant) == '1:1000'


def test_merge_genotypes_and_annotations():
    genotypes = pd.DataFrame([
        {
            'vcf_id': 'rs1',
        },
        {
            'vcf_id': '.',
            'vcf_chrom': '2',
            'vcf_pos': 2000,
        }
    ])

    annotations = pd.DataFrame([
        {
            'id': 'rs1',
            'ann': 'annotation-1',
            'rs-field': 'foo',
        },
        {
            'id': '2:2000',
            'ann': 'annotation-2',
            'all_vep_annotations': [{}],
        }
    ])

    result = merge_genotypes_and_annotations(genotypes, annotations)

    assert len(result) == len(genotypes)

    rsid_variant = result.iloc[0]
    assert rsid_variant['vcf_id'] == 'rs1'
    assert rsid_variant['ann'] == 'annotation-1'
    assert rsid_variant['rs-field'] == 'foo'

    loc_variant = result.iloc[1]
    assert loc_variant['id'] == '2:2000'
    assert loc_variant['vcf_id'] == '.'
    assert loc_variant['vcf_pos'] == 2000
