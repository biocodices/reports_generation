from reports_generation.pipeline import group_vep_annotations


def test_group_vep_annotations():
    vep_annotations = [
        {
            'impact': 'HIGH',
            'symbol': 'GENE1',
            'annotation': 'annotation-1'
        },
        {
            'impact': 'HIGH',
            'symbol': 'GENE1',
            'annotation': 'annotation-2'
        },
        {
            'impact': 'LOW',
            'symbol': 'GENE1',
            'annotation': 'annotation-3'
        },
    ]
    grouped = group_vep_annotations(vep_annotations)

    assert len(grouped['HIGH:GENE1']) == 2
    assert len(grouped['LOW:GENE1']) == 1
    assert grouped['HIGH:GENE1'][1]['annotation'] == 'annotation-2'

