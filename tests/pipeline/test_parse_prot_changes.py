from reports_generation.pipeline import parse_prot_changes


def test_parse_prot_changes():
    prot_changes = [
        'ENSP00000297494.3:p.Asp298Glu (Canonical Transcript)',
        'ENSP00000420215.1:p.Asp298Glu',
        'ENSP00000420551.1:p.Asp298Glu',
        'NP_000594.2:p.Asp298Glu',
        'NP_000594.2:p.Asp298Glu (Canonical Transcript)',
        'NP_001153581.1:p.Asp298Glu',
        'NP_001153582.1:p.Asp298Glu',
        'NP_001153583.1:p.Asp298Glu',
        'p.Asp298Glu',

        'ENSP00000417143.1:p.Asp92Glu', # Different
        'p.Glu298Asp', # Different
        'non-matching-prot-change', # This one should be kept
    ]

    result = parse_prot_changes(prot_changes)
    assert sorted(result) == [
        'non-matching-prot-change',
        'p.Asp298Glu',
        'p.Asp92Glu',
        'p.Glu298Asp',
    ]
