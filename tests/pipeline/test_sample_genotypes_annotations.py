import pytest

from reports_generation.pipeline import (
    define_zygosity,
    define_genotype_type,
    define_genotype_alleles,
    split_genotype_string,
)


@pytest.mark.parametrize('genotype,expected_zigosity', [
    ('0|0', 'Homozygous'),
    ('0', 'Hemizygous'),
    ('0|1', 'Heterozygous'),
    ('1|0', 'Heterozygous'),
    ('1|1', 'Homozygous'),
])
def test_define_zygosity(genotype, expected_zigosity):
    assert define_zygosity(genotype) == expected_zigosity


def test_define_genotype_type_and_alleles():
    variant = {
        'vcf_ref': 'A',
        'vcf_alt': ['G'],
        'genotype': '1|1',
        'zygosity': 'Homozygous'
    }
    assert define_genotype_type(variant) == 'Homozygous Alternative'
    assert define_genotype_alleles(variant) == 'G|G'


@pytest.mark.parametrize('geno_string,expected_alleles', [
    ('A|T', ['A', 'T']),
    ('A/A', ['A', 'A']),
    ('G', ['G']),
])
def test_split_genotype_string(geno_string, expected_alleles):
    assert split_genotype_string(geno_string) == expected_alleles

