import pandas as pd

from reports_generation.pipeline import sort_variants_by_worst_category


def test_sort_variants_by_worst_category():
    variants = pd.DataFrame([
        {'worst_category': 'BEN'},
        {'worst_category': 'PAT'},
        {'worst_category': 'LPAT'},
        {'worst_category': 'LBEN'},
    ])

    sorted_variants = sort_variants_by_worst_category(variants)
    assert all(sorted_variants['worst_category'] == ['PAT', 'LPAT', 'LBEN', 'BEN'])
    assert all(sorted_variants['order'] == [1, 2, 3, 4])

