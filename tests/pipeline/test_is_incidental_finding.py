from reports_generation.pipeline import is_incidental_finding


def test_is_incidental_finding():
    omim_entries = [
        {   # Incidental finding to be ignored
            'incidental_gene': False,
            'phenotypes': [
                {'incidental': True},
            ]
         },
        {   # Check if None is handled well
            'incidental_gene': True,
            'phenotypes': None,
        },
        {   # Incidental finding to be reported
            'incidental_gene': True,
            'phenotypes': [
                {},  # Pheno without incidental datum
                {'incidental': False},
                {'incidental': True},
            ]
         }
    ]

    assert is_incidental_finding(omim_entries)

    omim_entries.pop()
    assert not is_incidental_finding(omim_entries)

