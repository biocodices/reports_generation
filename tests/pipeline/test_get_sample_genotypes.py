import pytest

from reports_generation.pipeline import get_sample_genotypes


def test_get_sample_genotypes():
    vcf = pytest.helpers.file('genotypes.vcf')
    sample_id = 'HG00096'
    sample_genotypes = get_sample_genotypes(sample_id, vcf)

    assert list(sample_genotypes['sample_id']) == [sample_id] * 6
    assert list(sample_genotypes['genotype']) == \
        ['1|1', '0|0', '1|1', '0|1', '0|1', '0|1']

    expected_fields = [
        'vcf_chrom',
        'vcf_pos',
        'vcf_ref',
        'vcf_alt',
        'vcf_filter',
        'vcf_info',
        'genotype',
        'genotype_quality',
        'sample_id',
    ]
    for field in expected_fields:
        assert field in sample_genotypes

    assert sample_genotypes['FT'].iloc[0] is None  # Not NaN!

