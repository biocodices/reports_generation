import numpy as np

from reports_generation.pipeline import merge_frequencies


def test_merge_frequencies():
    variant = {
        'vcf_frequencies': {
            'A': {'AF': 0.1},
            'G': {'AF': 0.9},
        },
        'frequencies': {
            'A': {'CADD_1000g': {'AFR': 0.2}}
        }
    }

    result = merge_frequencies(variant)
    assert result == {
        'A': {
            'CADD_1000g': {'AFR': 0.2},
            'VCF': {'AF': 0.1},
        },
        'G': {
            'VCF': {'AF': 0.9}
        }
    }

    variant_with_nan = {
        'vcf_frequencies': {'A': {'AF': 0.1}},
        'frequencies': np.nan,
    }
    result = merge_frequencies(variant_with_nan)
    assert result == {'A': {'VCF': {'AF': 0.1}}}
