from reports_generation.pipeline import (
    parse_gwas_catalog_associations,
    fix_gwas_genomic_allele,
)


def test_parse_gwas_catalog_associations():
    gwas_association = {
        'rsids': ['rs1', 'rs2'],
        'genomic_alleles': {'rs1': 'A',
                            'rs2': 'T'},
        'allele_impacts': {'rs1': 'impact-1',
                           'rs2': 'impact-2'},
        'chrom_locations': {'rs2': 'chr2:2'},
        'urls': {'rs1': 'url-rs1'}
    }

    # The variant has ONE rs ID, the gwas associations mention many rs IDs
    variant = {'id': 'rs1',
               'ref': 'A',
               'alt': ['T'],
               'is_REV': False,
               'all_gwas_associations': [gwas_association]}

    # The tested method should keep only the annotations for the variant rs ID
    parsed_gwas_associations = parse_gwas_catalog_associations(variant)
    entry = parsed_gwas_associations[0]

    assert entry['rsid'] == 'rs1'
    assert entry['genomic_allele'] == 'A'
    assert entry['allele_impact'] == 'impact-1'
    assert entry['chrom_location'] is None
    assert entry['url'] == 'url-rs1'

    # Test if one key is missing the parsing doesn't break
    del(variant['all_gwas_associations'][0]['allele_impacts'])
    parsed_gwas_associations = parse_gwas_catalog_associations(variant)

    # Test when there is no data for the current rs ID
    variant['all_gwas_associations'] = [{'rsids': ['rs2'],
                                         'genomic_alleles': {},
                                         'allele_impacts': {},
                                         'chrom_locations': {},
                                         'urls': {}}]

    parsed_gwas_associations = parse_gwas_catalog_associations(variant)
    assert parsed_gwas_associations == []


def test_fix_gwas_genomic_allele():
    snp_alleles = ['C', 'T']
    gwas_associations = [{'genomic_allele': 'A'},
                         {'genomic_allele': 'T'}]
    result = fix_gwas_genomic_allele(gwas_associations, snp_alleles)

    assert result == [{'genomic_allele': 'T', 'genomic_allele_reversed': True},
                      {'genomic_allele': 'T'}]
