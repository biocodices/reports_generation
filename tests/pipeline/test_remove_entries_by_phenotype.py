from reports_generation.pipeline import remove_entries_by_phenotype


def test_remove_entries_by_phenotype():
    phenos_regex_list = [r'\brelevant\b']

    clinvar_variations_to_keep = [{'associated_phenotypes': ['Relevant Pheno']}]
    clinvar_variations_to_lose = [{'associated_phenotypes': ['Irrelevant Pheno']}]
    clinvar_entries_to_keep = [{'conditions': [{'name': 'RELEVANT'}]}]
    clinvar_entries_to_lose = [{'conditions': [{'name': 'IRRELEVANT'}]}]
    omim_entries_to_keep = [{'phenotypes': [{'name': 'relevant'}]}]
    omim_entries_to_lose = [{'phenotypes': [{'name': 'irrelevant'}]}]
    gwas_associations_to_keep = [{'trait': 'Very Relevant Type 2 (VR2)'}]
    gwas_associations_to_lose = [{'trait': 'Irrelevant'}]

    # When an entry has both relevant and irrelevant phenotypes,
    # we expect to get a parsed entry with just the relevant ones kept:
    mixed_phenos = [{'name': 'relevant'}, {'name': 'irrelevant'}]
    relevant_phenos = mixed_phenos[:1]

    mixed_omim_entry = {'phenotypes': mixed_phenos}
    parsed_mixed_omim_entry = {'phenotypes': relevant_phenos}
    mixed_clinvar_entry = {'conditions': mixed_phenos}
    parsed_mixed_clinvar_entry = {'conditions': relevant_phenos}

    mixed_clinvar_variation = {'associated_phenotypes': ['Relevant', 'Irrelevant']}
    parsed_mixed_clinvar_variation = {'associated_phenotypes': ['Relevant']}

    variant = {
        'reportable_clinvar_variations': (
            clinvar_variations_to_keep +
            [mixed_clinvar_variation] +
            clinvar_variations_to_lose
        ),
        'reportable_clinvar_entries': (
            clinvar_entries_to_keep +
            [mixed_clinvar_entry] +
            clinvar_entries_to_lose
        ),
        'omim_entries': (
            omim_entries_to_keep +
            [mixed_omim_entry] +
            omim_entries_to_lose
        ),
        'reportable_gwas_associations': (
            gwas_associations_to_keep +
            gwas_associations_to_lose
        ),
    }

    new_variant = remove_entries_by_phenotype(variant, phenos_regex_list)

    assert new_variant['reportable_clinvar_entries'] == \
        clinvar_entries_to_keep + [parsed_mixed_clinvar_entry]

    assert new_variant['reportable_clinvar_variations'] == \
        clinvar_variations_to_keep + [parsed_mixed_clinvar_variation]

    assert new_variant['omim_entries'] == \
        omim_entries_to_keep + [parsed_mixed_omim_entry]

    assert new_variant['reportable_gwas_associations'] == \
        gwas_associations_to_keep

    assert new_variant['excluded_phenotypes'] == \
        ['IRRELEVANT', 'Irrelevant', 'Irrelevant Pheno', 'irrelevant']

