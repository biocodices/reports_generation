from reports_generation.pipeline import filter_by_present_risk_alleles


def test_filter_by_present_risk_alleles():
    present_risk_alleles = ['A', 'G']
    annotations_to_include = [{'genomic_allele': 'A'},
                              {'genomic_allele': 'G'},
                              {'genomic_allele': None},
                              {}]
    annotations_to_exclude = [{'genomic_allele': 'T'},
                              {'genomic_allele': 'C'}]
    all_annotations = annotations_to_include + annotations_to_exclude

    variant = {'present_risk_alleles': present_risk_alleles,
               'ann_key': all_annotations}

    # Test it filters correctly for a SNP

    variant['vcf_info'] = {'VT': 'SNP'}
    filtered_annotations = \
        filter_by_present_risk_alleles(variant, annotations_key='ann_key')

    for annotation in annotations_to_include:
        assert annotation in filtered_annotations

    for annotation in annotations_to_exclude:
        assert annotation not in filtered_annotations

    # Test it doesn't apply any filters for non-SNPs
    # (we leave all entries just in case)

    variant['vcf_info']['VT'] = 'INDEL'
    filtered_annotations = \
        filter_by_present_risk_alleles(variant, annotations_key='ann_key')

    for annotation in all_annotations:
        assert annotation in filtered_annotations
