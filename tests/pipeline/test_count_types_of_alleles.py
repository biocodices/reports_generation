from reports_generation.pipeline import count_types_of_alleles


def test_count_types_of_alleles():
    categories = ['PAT', 'PAT', 'LPAT', 'BEN', 'BEN', 'BEN']
    counts = count_types_of_alleles(categories)
    assert counts[0] == ('PAT', 'Pathogenic', 2)
    assert counts[-1] == ('BEN', 'Benign', 3)

