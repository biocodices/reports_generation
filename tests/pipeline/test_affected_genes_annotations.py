import pandas as pd

from reports_generation.pipeline import affected_genes_annotations


def test_affected_genes_annotations():
    variants = pd.DataFrame([
        {
            'entrez_gene_ids': [1, 2],
            'all_vep_annotations': [
                {'gene': 1},
                {'gene': 'ENSG1'},
                {'gene': 3},
                {'gene': 'ENSG3'},
            ]
        },
    ])
    gene_annotations = pd.DataFrame([
        {'entrez_gene_id': 1},
        {'entrez_gene_id': 2},
        {'entrez_gene_id': 3},
        {'entrez_gene_id': 4},
    ])

    result = affected_genes_annotations(
        variants=variants,
        gene_annotations=gene_annotations,
    )

    result_ids = list(result['entrez_gene_id'])
    assert result_ids == [1, 2, 3]

