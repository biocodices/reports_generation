from reports_generation.pipeline import (
    generate_hgvs_g,
    add_ref_hgvs_g,
)


def test_generate_hgvs_g():
    variant = {
        'vcf_chrom': '1',
        'vcf_pos': '123',
        'vcf_ref': 'A',
        'vcf_alt': ['T'],
    }
    result = generate_hgvs_g(variant)
    assert result == [{'genomic_allele': 'T', 'hgvs_g': 'chr1:123A>T'}]

    variant['vcf_alt'].append('G')
    result = generate_hgvs_g(variant)
    assert len(result) == 2
    assert result[1] == {'genomic_allele': 'G', 'hgvs_g': 'chr1:123A>G'}


def test_add_ref_hgvs_g():
    variant = {
        'vcf_chrom': '1',
        'vcf_pos': '123',
        'vcf_ref': 'A',
        'hgvs': [{'genomic_allele': 'T'}],  # no 'A' (ref) entry
    }

    result = add_ref_hgvs_g(variant)
    expected_new_entry = {'genomic_allele': 'A', 'hgvs_g': 'chr1:123A='}
    assert expected_new_entry in result

