from reports_generation.pipeline import remove_haplotype_associations


def test_remove_haplotype_associations():
    multi = {'multisnp_haplotype': True}
    other = {}
    gwas_associations = [multi, other]

    result = remove_haplotype_associations(gwas_associations)
    assert multi not in result
    assert other in result

