import pandas as pd

from reports_generation.pipeline import add_VEP_genes


def test_add_VEP_genes():
    variant = pd.Series({
        'id': 1,
        'entrez_gene_symbols': ['Gene-1', 'Gene-2'],
        'all_vep_annotations': [
            {'symbol': 'Gene-3'},
            {'symbol': 'Gene-1'},
            {'symbol': 'Gene-3'},
            {'symbol': 'Gene-1'},
            {'symbol': None},
        ]
    })

    result = add_VEP_genes(variant)
    assert result == ['Gene-1', 'Gene-2', 'Gene-3']

