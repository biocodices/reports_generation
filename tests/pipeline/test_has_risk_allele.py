import pytest
import numpy as np

from reports_generation.pipeline import (
    has_risk_allele,
    present_risk_alleles,
)


@pytest.mark.parametrize('variant_type,risk_alleles,genotype_alleles,expected_result', [
    ('SNP', ['A'], ['G', 'G'], False),
    ('SNP', ['A'], ['G', 'A'], True),

    # When there are no risk alleles, return None:
    ('SNP', [], ['G', 'G'], None),
    ('SNP', None, ['G', 'G'], None),

    # When it's not a SNP, return None
    ('INDEL', ['TC'], ['T', 'TC'], None),
    ('SV', ['<CN2>'], ['G', '<CN2>'], None),
])
def test_has_risk_allele(variant_type, risk_alleles, genotype_alleles,
                         expected_result):
    variant = {
        'risk_alleles': risk_alleles,
        'genotype_alleles': genotype_alleles,
        'vcf_info': {'VT': variant_type}
    }
    # Test explicitly for boolean results, not just truthy or falsey,
    # and test uncertainty as a None result:
    assert has_risk_allele(variant) is expected_result

    with pytest.raises(ValueError):
        has_risk_allele({'risk_alleles': np.nan})


@pytest.mark.parametrize('variant,expected_present_alleles', [
    ({'risk_alleles': ['T'], 'genotype_alleles': ['A', 'A']}, []),
    ({'risk_alleles': ['A'], 'genotype_alleles': ['A', 'A']}, ['A']),
    ({'risk_alleles': ['T'], 'genotype_alleles': ['A', 'T']}, ['T']),
    ({'risk_alleles': ['T', 'C'], 'genotype_alleles': ['C']}, ['C']),
])
def test_present_risk_alleles(variant, expected_present_alleles):
    assert present_risk_alleles(variant) == expected_present_alleles

