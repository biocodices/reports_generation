import pandas as pd

from reports_generation.pipeline import prepare_sample_genotypes


def test_prepare_sample_genotypes():
    annotated_genotypes = pd.DataFrame([{
        'id': 'rs1',
        'vcf_chrom': '1',
        'vcf_pos': 123,
        'sample_id': 'S1',
        'vcf_ref': 'A',
        'vcf_alt': ['T'],
        'vcf_info': {'VT': 'SNP', 'AF_Population-2': 0.1},
        'risk_alleles': ['T'],
        'read_depth': 100,
        'allele_depth': [50, 50],
        'genotype': '0|1',
        'hgvs': [],
        'frequencies': {'T': {'Some-Source': {'Population-1': 0.15}}},

        'all_snpeff_annotations': [],
        'all_vep_annotations': [],

        # Create this clinvar variation so that one variant will be reportable
        'all_clinvar_variations': [{
            'genomic_allele': 'T',
            'clinical_significances': ['Pathogenic'],
            'coding_changes': ['c.1A>T'],
            'associated_phenotypes': []
        }],

        'all_clinvar_entries': [],
        'all_gwas_associations': [],

        'clinvar_worst_significance': 'PAT',
        'omim_entries': [],
        'uniprot_entries': [],
    }])

    prepared_genotypes = prepare_sample_genotypes(
        annotated_genotypes,
        sample_id='S1',
        min_reportable_category='HIGH',
        min_odds_ratio=1,
        max_frequency=1,
        phenos_regex_list=[]
    )

    # Test that the frequencies from the VCF info field have been merged into
    # the 'frequencies' column:
    geno = prepared_genotypes.loc[0]
    assert geno['frequencies']['T']['Some-Source']['Population-1'] == 0.15
    assert geno['frequencies']['T']['VCF']['AF_Population-2'] == 0.1

    expected_columns = [
        'zygosity',
        'genotype_type',
        'genotype_alleles_string',
        'genotype_alleles',
        'has_risk_allele',
        'present_risk_alleles',
        'sample_hgvs',
        'allele_depth_ratios',

        'frequencies',
        'vcf_frequencies',

        'sample_snpeff_annotations',
        'reportable_snpeff_annotations',
        'grouped_snpeff_annotations',

        'sample_vep_annotations',
        'reportable_vep_annotations',

        'sample_clinvar_variations',
        'reportable_clinvar_variations',

        'sample_clinvar_entries',
        'reportable_clinvar_entries',
        'grouped_clinvar_entries',

        'sample_gwas_associations',
        'reportable_gwas_associations',

        'clinvar_significances',
        'clinvar_worst_significance',
        'snpeff_impacts',
        'snpeff_worst_impact',
        'worst_category',
        'worst_category_description',
        'clinvar_phenotype_names',
        'omim_phenotype_names',
        'phenotype_names',
        'cds_changes',
        'parsed_cds_changes',
        'prot_changes',
        'parsed_prot_changes',
    ]

    for column in expected_columns:
        assert column in prepared_genotypes
