from reports_generation.pipeline import parse_cds_changes


def test_parsed_cds_changes():
    cds_changes = [
        'non-matching-cds-change',
        'NM_000311.4:c.385A>G',
        'NM_000311.4:c.385A>G (Canonical Transcript)',
        'NM_001271561.1:c.*74A>G',
        'c.385A>G',
    ]

    result = parse_cds_changes(cds_changes)
    assert sorted(result) == [
        'c.*74A>G',
        'c.385A>G',
        'c.385A>G (Canonical Transcript)',
        'non-matching-cds-change',
    ]
