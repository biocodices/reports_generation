from reports_generation.pipeline import extract_clinvar_comments


def test_extract_clinvar_comments():
    comment1 = { 'data_source': 'Source-1',
                'type': 'comment-type-1',
                'text': 'Comment-1 about the variant.'}
    comment2 = { 'data_source': 'Source-2',
                'type': 'comment-type-2',
                'text': 'Comment-2 about the variant.' }
    comment3 = { 'data_source': 'Source-3',
                'type': 'comment-type-3',
                'text': 'Comment-3 about the variant.' }

    variations = [
        {'clinical_assertions': [
            {'clinical_significance_detail': {'comments': [comment1]}},
            {'clinical_significance_detail': {'comments': [comment2]}}]},
        {'clinical_assertions': [
            {'clinical_significance_detail': {'comments': [comment3]}}]}
    ]
    result = extract_clinvar_comments(variations)
    assert result == [
        '[Source-1 | comment-type-1] Comment-1 about the variant.',
        '[Source-2 | comment-type-2] Comment-2 about the variant.',
        '[Source-3 | comment-type-3] Comment-3 about the variant.',
    ]
