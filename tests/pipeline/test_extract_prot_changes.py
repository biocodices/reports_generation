from reports_generation.pipeline import extract_prot_changes


def test_extract_prot_changes():
    variant = {
        'sample_clinvar_variations': [
            {'protein_changes': ['change-A', 'change-B']},
            {'protein_changes': ['change-C']},
            {} # Has no protein_changes key
        ],
        'sample_clinvar_entries': [
            {'prot_change': 'change-1'},
            {'prot_change': 'change-2'}
        ],
        'omim_entries': [
            {'prot_changes': ['change-1', 'change-3']}
        ],
        'uniprot_entries': [
            {'prot_change': 'change-1'},
            {'prot_change': 'change-4'}
        ],
        'sample_snpeff_annotations': [
            {'hgvs_p': 'change-1'},
            {'hgvs_p': 'change-5'}
        ],
        'sample_vep_annotations': [
            {'hgvsp': None, 'canonical': None},
            {'hgvsp': None, 'canonical': 'YES'},
            {'hgvsp': 'change-1', 'canonical': None},
            {'hgvsp': 'change-6', 'canonical': 'YES'}
        ],
    }
    expected_values = [
        'change-1',
        'change-2',
        'change-3',
        'change-4',
        'change-5',
        'change-6 (Canonical Transcript)',
        'change-A',
        'change-B',
        'change-C'
    ]
    assert extract_prot_changes(variant) == expected_values

    keys = ['omim_entries', 'sample_clinvar_entries', 'uniprot_entries',
            'sample_snpeff_annotations', 'sample_vep_annotations',
            'sample_clinvar_variations']
    for key in keys:
        variant[key] = []

    assert extract_prot_changes(variant) == []
