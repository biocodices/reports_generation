from reports_generation.pipeline import compute_allele_depth_ratios


def test_compute_allele_depth_ratios():
    variant = {
        'read_depth': 100,
        'allele_depth': [40, 50]
    }
    result = compute_allele_depth_ratios(variant)
    assert result == [.4, .5]
