import pandas as pd

from reports_generation.pipeline import count_incidental_findings


def test_count_incidental_findings():
    variants = pd.DataFrame([{'is_incidental': False}] * 5 +
                            [{'is_incidental': True}] * 2)
    assert count_incidental_findings(variants) == 2

