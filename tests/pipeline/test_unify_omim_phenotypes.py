import pytest

from reports_generation.pipeline import unify_omim_phenotypes


def test_unify_omim_phenotypes():
    with pytest.raises(TypeError):
        unify_omim_phenotypes(None)

    assert unify_omim_phenotypes([]) == []

    omim_entries = [
        {
            'phenotypes': [
                {'name': 'Pheno-1'},
                {'name': 'Pheno-2'}
            ],
            'phenotype_names': [
                'PHENO-1',  # Dupe
                'PHENO-3'  # New
            ]
        }
    ]

    new_entries = unify_omim_phenotypes(omim_entries)
    assert {'name': 'PHENO-3'} in new_entries[0]['phenotypes']

    # Test that it doesn't modify the original entries:
    assert {'name': 'PHENO-3'} not in omim_entries[0]['phenotypes']

