import pytest
import pandas as pd
import numpy as np

from reports_generation.pipeline import extract_cds_strand


@pytest.mark.parametrize('variant,expected_strand', [
    # DBNSFP data is read ok
    ({'dbnsfp': [{'cds_strand': '+'}]}, '+'),
    ({'dbnsfp': [{'cds_strand': '-'}]}, '-'),
    ({'dbnsfp': [{'cds_strand': '-'},
                 {'cds_strand': '-'}]}, '-'),

    # Break if DbNSFP has contradictory data
    ({'dbnsfp': [{'cds_strand': '-'},
                 {'cds_strand': '+'}]}, 'FAIL'),

    # Ignore VEP contradictory data
    ({'all_vep_annotations': [{'strand': -1},
                              {'strand': 1}]}, None),

    # VEP data is parsed ok
    ({'all_vep_annotations': [{'strand': 1}]}, '+'),
    ({'all_vep_annotations': [{'strand': -1}]}, '-'),
    ({'all_vep_annotations': [{'strand': '1'}]}, '+'),
    ({'all_vep_annotations': [{'strand': '-1'}]}, '-'),

    # Prefer DBNSFP to VEP if they don't match
    ({'id': 1,  # ID is used in a warning
      'all_vep_annotations': [{'strand': 1}],
      'dbnsfp': [{'cds_strand': '-'}]}, '-'),
    ({'id': 1,  # ID is used in a warning
      'all_vep_annotations': [{'strand': -1}],
      'dbnsfp': [{'cds_strand': '+'}]}, '+'),

    # Consistent VEP + DBNSDP data is read ok
    ({'all_vep_annotations': [{'strand': 1}],
      'dbnsfp': [{'cds_strand': '+'}]}, '+'),
    ({'all_vep_annotations': [{'strand': -1}],
      'dbnsfp': [{'cds_strand': '-'}]}, '-'),

    # No data cases are handled
    ({'strand': np.nan}, None),
    ({'all_vep_annotations': [{'strand': np.nan}]}, None),
    ({}, None),
    ({'dbnsfp': None}, None),
    ({'dbnsfp': [{}]}, None),

])
def test_extract_cds_strand(variant, expected_strand):
    variant = pd.Series(variant)

    if expected_strand == 'FAIL':
        with pytest.raises(Exception):
            extract_cds_strand(variant)
    else:
        assert extract_cds_strand(variant) == expected_strand

