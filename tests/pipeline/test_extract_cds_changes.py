from reports_generation.pipeline import extract_cds_changes


def test_extract_cds_changes():
    variant = {
        'reportable_clinvar_variations': [
            {'coding_changes': ['change-A', 'change-B']},
            {'coding_changes': ['change-C']}
        ],
        'reportable_clinvar_entries': [
            {'cds_change': 'change-1'},
            {'cds_change': 'change-1'},
            {'cds_change': 'change-2'},
        ],
        'reportable_snpeff_annotations': [
            {'hgvs_c': 'change-1'},
            {'hgvs_c': 'change-3'},
        ],
        'reportable_vep_annotations': [
            {'hgvsc': None, 'canonical': None},
            {'hgvsc': None, 'canonical': 'YES'},
            {'hgvsc': 'change-1', 'canonical': None},
            {'hgvsc': 'change-4', 'canonical': 'YES'},
        ]
    }

    assert extract_cds_changes(variant) == [
        'change-1', 'change-2', 'change-3',
        'change-4 (Canonical Transcript)',
        'change-A', 'change-B', 'change-C',
    ]
