import pytest

from reports_generation.pipeline import snp_alleles_reported_reverse


@pytest.mark.parametrize('g37_rev,g38_rev,expected_result', [
    (True, False, True),
    (False, True, True),
    (False, False, False),
    (True, True, True),
    (None, True, True),
    (False, None, False),
    (None, None, False),
])
def test_snp_alleles_reported_reverse(g37_rev, g38_rev, expected_result):
    dbsnp_web = {}

    if g37_rev:
        dbsnp_web['GRCh37.p13_reverse'] = g37_rev

    if g38_rev:
        dbsnp_web['GRCh38.p7_reverse'] = g38_rev

    result = snp_alleles_reported_reverse(dbsnp_web)
    assert result is expected_result

    assert snp_alleles_reported_reverse(False) is False

