import pytest
import numpy as np
import pandas as pd

from reports_generation.pipeline import infer_risk_alleles


@pytest.mark.parametrize(
    'clinvar_variations,clinvar_entries,snpeff_annotations,gwas_associations,vep_annotations,expected_alleles',
    [
        # Consistent data from different entries:
        ([{'genomic_allele': 'A'}],
         [{'genomic_allele': 'A'}],
         [{'genomic_allele': 'A'}],
         [{'genomic_allele': 'A'}],
         [{'genomic_allele': 'A'}],
         ['A']),

        # Different data from different entries:
        ([{'genomic_allele': 'A'}],
         [{'genomic_allele': 'B'}],
         [{'genomic_allele': 'C'}],
         [{'genomic_allele': 'D'}],
         [{'genomic_allele': 'E'}],
         ['A', 'B', 'C', 'D', 'E']),

        # Only clinvar variations data:
        ([{'genomic_allele': 'A'}], [], [], [], [], ['A']),

        # Only clinvar entries data:
        ([], [{'genomic_allele': 'A'}], [], [], [], ['A']),

        # Only snpeff data:
        ([], [], [{'genomic_allele': 'A'}], [], [], ['A']),

        # Only gwas catalog data:
        ([], [], [], [{'genomic_allele': 'A'}], [], ['A']),

        # Only VEP data:
        ([], [], [], [], [{'genomic_allele': 'A'}], ['A']),

        # Gwas adds a None value:
        ([], [], [{'genomic_allele': 'A'}], [{'genomic_allele': None}], [], ['A']),

        # No data:
        ([{}], [], [], [], [], []),
        (None, None, None, None, None, []),
        (np.nan, np.nan, np.nan, np.nan, np.nan, [])
    ]
)
def test_infer_risk_alleles(clinvar_variations, clinvar_entries,
                            snpeff_annotations, gwas_associations,
                            vep_annotations, expected_alleles):
    variant = pd.Series({
        'id': 'rs1',
        'all_clinvar_variations': clinvar_variations,
        'all_clinvar_entries': clinvar_entries,
        'all_gwas_associations': gwas_associations,
        'all_vep_annotations': vep_annotations,
        'all_snpeff_annotations': snpeff_annotations,
    })

    assert infer_risk_alleles(variant) == expected_alleles
