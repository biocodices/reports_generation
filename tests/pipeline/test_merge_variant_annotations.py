import pandas as pd

from reports_generation.pipeline import merge_variant_annotations

def test_merge_variant_annotations():
    annotations = pd.DataFrame([
        {
            'id': 'rs1',
            'annotations': 'ann-1',
        },
    ])
    vep_annotations = pd.DataFrame([
        {
            'id_or_location': 'rs1',
            'vep_annotations': ['vep-ann-1', 'vep-ann-2']
        },
        {
            'id_or_location': '1:999',
            'vep_annotations': ['vep-ann-3'],
        },
    ])

    result = merge_variant_annotations(annotations, vep_annotations)

    assert len(result) == 2
    assert list(result['id_or_location']) == ['rs1', '1:999']
    assert result.set_index('id_or_location').\
            loc['1:999', 'vep_annotations'] == ['vep-ann-3']

    result = result.set_index('id_or_location')

    # A variant with the same regular rs ID is merged correctly:
    assert result.loc['rs1', 'annotations'] == 'ann-1'
    assert result.loc['rs1', 'vep_annotations'] == ['vep-ann-1', 'vep-ann-2']

    # A variant not present in annotations but present in VEP is merged
    # correctly:
    assert result.loc['1:999', 'vep_annotations'] == ['vep-ann-3']
    assert result.loc['1:999', 'annotations'] is None  # i.e. not NaN!

