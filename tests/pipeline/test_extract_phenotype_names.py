import pytest
import pandas as pd

from reports_generation.pipeline import (
    extract_phenotype_names,
    extract_omim_phenotype_names,
    extract_clinvar_phenotype_names,
    extract_clinvar_variation_phenotype_names,
    extract_gwas_phenotype_names,
)


CLINVAR_ENTRIES = [{'conditions': [{'name': 'not provided'},
                                   {'name': 'not specified'},
                                   {'name': 'Pheno-1'}]},

                   {'conditions': [{'name': 'Pheno-1'},
                                   {'name': 'Pheno-2'}]},

                   {'conditions': [{'name': None}, {}]}]

CLINVAR_VARIATIONS = [
    {'associated_phenotypes': ['Pheno-1', 'Pheno-2', 'not provided']},
    {'associated_phenotypes': ['Pheno-1', 'Pheno-3']},
    {'associated_phenotypes': ['not specified']}
]

OMIM_ENTRIES = [{'phenotypes': [{'name': 'not specified'},
                                {'name': 'Pheno-1'}]},

                {'phenotypes': [{'name': 'Pheno-3'},
                                {'name': None},
                                {}]}]

GWAS_ENTRIES = [{'trait': 'Pheno-3'},
                {'trait': 'Pheno-3'},
                {'trait': 'Pheno-4'}]


def test_extract_omim_phenotype_names():
    with pytest.raises(TypeError):
        extract_omim_phenotype_names(None)

    assert extract_omim_phenotype_names([]) == []
    assert extract_omim_phenotype_names(OMIM_ENTRIES) == ['Pheno-1', 'Pheno-3']


def test_extract_clinvar_phenotype_names():
    with pytest.raises(TypeError):
        extract_clinvar_phenotype_names(None)

    assert extract_clinvar_phenotype_names([]) == []
    assert extract_clinvar_phenotype_names(CLINVAR_ENTRIES) == ['Pheno-1',
                                                                'Pheno-2']


def test_extract_clinvar_variation_phenotype_names():
    with pytest.raises(TypeError):
        extract_clinvar_variation_phenotype_names(None)

    assert extract_clinvar_variation_phenotype_names([]) == []
    assert extract_clinvar_variation_phenotype_names(CLINVAR_VARIATIONS) == \
        ['Pheno-1', 'Pheno-2', 'Pheno-3']


def test_extract_gwas_phenotype_names():
    with pytest.raises(TypeError):
        extract_gwas_phenotype_names(None)

    assert extract_gwas_phenotype_names([]) == []
    assert extract_gwas_phenotype_names(GWAS_ENTRIES) == ['Pheno-3', 'Pheno-4']


def test_extract_phenotype_names():
    variant = pd.Series({
        'clinvar_variation_phenotype_names': [],
        'clinvar_phenotype_names': [],
        'omim_phenotype_names': [],
        'gwas_phenotype_names': [],
    })
    assert extract_phenotype_names(variant) == []

    variant['clinvar_phenotype_names'] = ['Pheno-1']
    assert extract_phenotype_names(variant) == ['Pheno-1']

    variant['clinvar_variation_phenotype_names'] = ['Pheno-2']
    assert extract_phenotype_names(variant) == ['Pheno-1', 'Pheno-2']

    variant['omim_phenotype_names'] = ['Pheno-2', 'Pheno-3']
    assert extract_phenotype_names(variant) == ['Pheno-1', 'Pheno-2', 'Pheno-3']

    variant['clinvar_phenotype_names'] = []
    assert extract_phenotype_names(variant) == ['Pheno-2', 'Pheno-3']

    variant['gwas_phenotype_names'] = ['Pheno-4']
    assert extract_phenotype_names(variant) == ['Pheno-2', 'Pheno-3', 'Pheno-4']
