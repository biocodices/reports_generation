from reports_generation.pipeline import extract_gmaf


def test_extract_gmaf():
    ensembl_annotation = {'MAF': 0.002}
    assert extract_gmaf(ensembl_annotation) == 0.002

    ensembl_annotation = {}
    assert extract_gmaf(ensembl_annotation) is None

