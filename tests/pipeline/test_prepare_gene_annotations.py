import pandas as pd
import pytest

from reports_generation.pipeline import prepare_gene_annotations


def test_prepare_gene_annotations():
    original_df = pd.read_json(pytest.helpers.file('genes.json'),
                               orient='split')
    new_df = prepare_gene_annotations(original_df)

    for field in 'symbol name description url'.split():
        assert field in new_df
        assert field not in original_df


def test_prepare_gene_annotations_2():
    # This is an improved version of the test above, because it manually
    # creates the situation that should be tested, instead of relying on an
    # unreadable JSON file elsewhere.
    # However, the test above has many real annotations with variations that
    # would be cumbersome to reproduce manually, so we're stuck with both
    # tests I guess. :/

    gene_annotations = pd.DataFrame([{
        'entrez_gene_id': '1',
        'mygene': {
            'MIM': '100',
            'swissprot': 'SP1',
            'HGNC': '200'
        },
        'gene_entrez': {
            'summary': 'Gene summary',
            'nomenclature_name': 'Gene-1',
            'nomenclature_symbol': 'Gene-Symbol-A',
            'name': 'Gene-Symbol-B',
            'url': 'Gene URL',
            'other_aliases': 'Alias-1, Alias-2',
            'map_location': 'Gene-Loc',
        }
    }])

    df = prepare_gene_annotations(gene_annotations)

    gene = df.iloc[0]

    assert gene['entrez_gene_id'] == '1'
    assert gene['symbol'] == 'Gene-Symbol-A'
    assert gene['name'] == 'Gene-1'
    assert gene['description'] == 'Gene summary'
    assert gene['other_names'] == 'Alias-1, Alias-2'
    assert gene['location'] == 'Gene-Loc'
    assert gene['url'] == 'Gene URL'
    assert gene['mim'] == '100'
    assert gene['swissprot'] == 'SP1'
    assert gene['hgnc'] == '200'

    # It shouldn't break if one of the dictionaries is missing
    gene_annotations.loc[0, 'mygene'] = None
    prepare_gene_annotations(gene_annotations)

