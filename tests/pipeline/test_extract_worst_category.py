import pytest

from reports_generation.pipeline import extract_worst_category


@pytest.mark.parametrize(
    'categories,expected_worst',
    [
        (['LOW', 'LOW', 'PAT'], 'PAT'),
        (['PAT', 'MODERATE', 'HIGH'], 'PAT'),
    ]
)
def test_extract_worst_category(categories, expected_worst):
    assert extract_worst_category(categories) == expected_worst

