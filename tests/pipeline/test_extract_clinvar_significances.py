import pytest

from reports_generation.pipeline import (
    extract_clinvar_significances,
    clinvar_worst_significance,
)


@pytest.mark.parametrize(
    'clinical_significances,expected_counts',
    [
        (['Likely benign', 'Pathogenic|Likely pathogenic'],
         {'LBEN': 1, 'PAT': 1, 'LPAT': 1}),

        (['Benign|risk factor', 'Likely benign', 'Likely benign'],
         {'BEN': 1, 'RISKF': 1, 'LBEN': 2}),

        (['Uncertain significance', 'Pathogenic', 'risk factor', 'Pathogenic'],
         {'UNCERT': 1, 'PAT': 2, 'RISKF': 1}),

        (['Likely benign', 'risk factor', 'drug response', 'association'],
         {'LBEN': 1, 'RISKF': 1, 'DRUG': 1, 'ASSOC': 1}),

        (['Conflicting interpretations of pathogenicity|other', 'not provided'],
         {'CONFL': 1, 'NA': 1, 'OTHER': 1})
    ]
)
def test_clinvar_significances(clinical_significances, expected_counts):
    variant = {
        'clinvar_variations': [{'clinical_significances': sig.split('|')}
                               for sig in clinical_significances]
    }

    sig_counts = extract_clinvar_significances(variant['clinvar_variations'])
    assert sig_counts == expected_counts


def test_clinvar_worst_significance():
    sig_counts = {'Likely pathogenic': 2}
    assert clinvar_worst_significance(sig_counts) == 'LPAT'

    sig_counts['risk factor'] = 3
    assert clinvar_worst_significance(sig_counts) == 'LPAT'

    sig_counts['Pathogenic'] = 1
    assert clinvar_worst_significance(sig_counts) == 'PAT'
