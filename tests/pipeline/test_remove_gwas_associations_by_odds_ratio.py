from reports_generation.pipeline import remove_gwas_associations_by_odds_ratio


def test_remove_gwas_associations_by_odds_ratio():
    gwas_associations_to_keep = [{'odds_ratio_per_copy': 1.5}, {}]
    gwas_associations_to_remove = [{'odds_ratio_per_copy': 1.1}]

    result = remove_gwas_associations_by_odds_ratio(
        gwas_associations_to_keep + gwas_associations_to_remove,
        min_odds_ratio=1.2,
    )

    for association in gwas_associations_to_keep:
        assert association in result

    for association in gwas_associations_to_remove:
        assert association not in result

