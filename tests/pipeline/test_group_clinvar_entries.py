from copy import deepcopy

from reports_generation.pipeline import (
    group_clinvar_entries,
)


def test_group_clinvar_entries():
    assert group_clinvar_entries(False) == {}
    assert group_clinvar_entries(None) == {}

    entry1 = {
        'accession': 'RCV1',
        'transcript': 'NM_1.1',
        'cds_change': 'c.1A>G',
        'prot_change': 'p.Ala1Gly',
        # 'gene': 'TTN',
        'gene_symbol': 'TTN',
        'clinical_significances': ['Pathogenic', 'risk factor'],
        'url': '',
        'conditions': [
            {'name': 'Pheno-1a'},
            {'name': 'Pheno-1b'},
            {'name': 'not provided'}
        ]
    }

    entry2 = deepcopy(entry1)
    entry2['accession'] = 'RCV2'
    entry2['clinical_significances'] = ['Benign']
    entry2['conditions'] = [{'name': 'Pheno-2'}]

    entry3 = deepcopy(entry1)
    entry3['cds_change'] = 'c.1A>C'  # Different allele!
    entry3['clinical_significances'] = ['Likely pathogenic']
    entry3['accession'] = 'RCV3'
    entry3['conditions'] = [{'name': 'not provided'}]

    entry4 = deepcopy(entry3)
    entry4['accession'] = 'RCV4'
    entry4['clinical_significances'] = ['Uncertain significance']
    entry4['conditions'] = [{'name': 'Pheno-4'}]

    clinvar_entries = [entry1, entry2, entry3, entry4]
    grouped = group_clinvar_entries(clinvar_entries)

    keys = list(grouped.keys())
    assert keys == ['NM_1.1:TTN:c.1A>G:p.Ala1Gly',
                    'NM_1.1:TTN:c.1A>C:p.Ala1Gly']

    assert list(grouped[keys[0]].keys()) == ['Pathogenic', 'Benign']
    assert list(grouped[keys[1]].keys()) == ['Likely pathogenic',
                                             'Uncertain significance']

