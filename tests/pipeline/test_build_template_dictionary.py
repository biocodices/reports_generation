import pandas as pd

from reports_generation.pipeline import (
    build_template_dictionary,
    gather_excluded_phenotypes,
)


def test_build_template_dictionary():
    sample_id = 'sample-X'
    genes = pd.DataFrame([
        {'entrez_id': 1}
    ])
    variants = pd.DataFrame([
        {'worst_category': 'PAT',
         'is_incidental': True,
         'excluded_phenotypes': [],
         'omim_entries': [],
         'all_gwas_associations': []},

        {'worst_category': 'LPAT',
         'is_incidental': False,
         'excluded_phenotypes': [],
         'omim_entries': [],
         'all_gwas_associations': []}
    ])
    thresholds = {}

    template_data = build_template_dictionary(sample_id=sample_id,
                                              variants=variants,
                                              genes=genes,
                                              thresholds=thresholds)

    keys_to_expect = [
        'category_counts',
        'incidental_findings_count',
        'sample',
        'variants',
        'genes',
        'references',
        'thresholds',
    ]
    for key in keys_to_expect:
        assert key in template_data


def test_gather_excluded_phenotypes():
    variants = pd.DataFrame([
        {'excluded_phenotypes': ['pheno-1', 'pheno-2']},
        {'excluded_phenotypes': []},
        {'excluded_phenotypes': ['pheno-1', 'pheno-3']},
    ])

    excluded_phenotypes = gather_excluded_phenotypes(variants)
    assert excluded_phenotypes == ['pheno-1', 'pheno-2', 'pheno-3']

