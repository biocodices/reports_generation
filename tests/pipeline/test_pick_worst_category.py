import pytest

from reports_generation.pipeline import pick_worst_category


def test_pick_worst_category():
    variant = {}

    with pytest.raises(KeyError):
        assert pick_worst_category(variant) == None

    variant['sample_gwas_associations'] = []
    variant['clinvar_worst_significance'] = 'BEN'
    variant['snpeff_worst_impact'] = 'LOW'
    variant['vep_worst_impact'] = 'LOW'
    assert pick_worst_category(variant).tag == 'LOW'

    variant['vep_worst_impact'] = 'MODERATE'
    assert pick_worst_category(variant).tag == 'MODERATE'

    variant['sample_gwas_associations'] = [{}]  # has one association
    assert pick_worst_category(variant).tag == 'ASSOC'

    variant['snpeff_worst_impact'] = 'HIGH'
    assert pick_worst_category(variant).tag == 'HIGH'

    variant['clinvar_worst_significance'] = 'PAT'
    assert pick_worst_category(variant).tag == 'PAT'

    variant['sample_gwas_associations'] = []
    variant['clinvar_worst_significance'] = []
    variant['snpeff_worst_impact'] = []
    variant['vep_worst_impact'] = []
    assert pick_worst_category(variant).tag == 'NA'

