import pandas as pd

from reports_generation.pipeline import gather_pubmed_references


def test_gather_pubmed_references():
    variants = pd.DataFrame([
        {  # Variant 1
            'order': 1,
            'omim_entries': [
                {'pubmed_entries': [{'pmid': '1', 'AMA_Citation': 'Ref-1'},
                                    {'pmid': '2', 'AMA_Citation': 'Ref-2'}]},

                {'pubmed_entries': [{'pmid': '1', 'AMA_Citation': 'Ref-1'},
                                    {'pmid': '3', 'AMA_Citation': 'Ref-3'}]},

                {'pubmed_entries': []},
                {'pubmed_entries': None},
            ],
            'all_gwas_associations': [],
        },

        {  # Variant 2
            'order': 2,
            'omim_entries': [
                {'pubmed_entries': [{'pmid': '1', 'AMA_Citation': 'Ref-1'},
                                    {'pmid': '4', 'AMA_Citation': 'Ref-4'}]},
            ],
            'all_gwas_associations': [
                {'pubmed_entries': [{'pmid': '5', 'AMA_Citation': 'Ref-5'}]}
            ]
        },

        { # Variant 3
            'order': 3,
            'omim_entries': [
                {'pubmed_entries': [{'pmid': '3', 'AMA_Citation': 'Ref-3'}]},
            ],
         'all_gwas_associations': []
         }
    ])

    pubmed_references = gather_pubmed_references(variants)
    assert [r['pmid'] for r in pubmed_references] == ['1', '2', '3', '4', '5']

    variant_1_references = [r for r in pubmed_references if 1 in r['variants']]
    assert len(variant_1_references) == 3
    assert [r['pmid'] for r in variant_1_references] == ['1', '2', '3']

    variant_2_references = [r for r in pubmed_references if 2 in r['variants']]
    assert len(variant_2_references) == 3
    assert [r['pmid'] for r in variant_2_references] == ['1', '4', '5']

    variant_3_references = [r for r in pubmed_references if 3 in r['variants']]
    assert len(variant_3_references) == 1
    assert [r['pmid'] for r in variant_3_references] == ['3']
