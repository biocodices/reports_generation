import pandas as pd

from reports_generation.pipeline import select_sample_variants


def test_select_sample_variants():
    vm = VariantMaker('sample-X')

    # I thoroughly test here the logic of select_sample_variants
    # --all combinations of selection criteria are tested.
    # Check the method's docstring for the rationale of the selection.

    variants_to_include = [
        vm.make_variant(has_reportable_clinvar_variations=True),
        vm.make_variant(has_reportable_gwas_entries=True),
        vm.make_variant(has_no_clinvar_data_at_all=True,
                        has_omim_entries=True),
        vm.make_variant(has_no_clinvar_data_at_all=True,
                        has_reportable_snpeff_entries=True),
        vm.make_variant(has_no_clinvar_data_at_all=True,
                        has_reportable_vep_entries=True)
    ]

    variants_to_exclude = [
        vm.make_variant(),

        # We trust ClinVar variations over separated ClinVar entries:
        vm.make_variant(has_reportable_clinvar_variations=False,
                        has_reportable_clinvar_entries=True),

        vm.make_variant(has_no_clinvar_data_at_all=True,
                        has_reportable_snpeff_entries=False,
                        has_omim_entries=False,
                        has_reportable_vep_entries=False),
        vm.make_variant(has_no_clinvar_data_at_all=False,
                        has_reportable_snpeff_entries=True,
                        has_omim_entries=True,
                        has_reportable_vep_entries=True)
    ]

    variants = pd.DataFrame(variants_to_include + variants_to_exclude)
    selected = select_sample_variants(variants)

    for variant in variants_to_include:
        assert variant['id'] in selected['id'].values

    for variant in variants_to_exclude:
        assert variant['id'] not in selected['id'].values


class VariantMaker():
    variants_count = 0

    def __init__(self, sample_id):
        self.sample_id = sample_id

    def make_variant(self,
                     has_reportable_clinvar_variations=False,
                     has_reportable_clinvar_entries=False,
                     has_reportable_gwas_entries=False,
                     has_no_clinvar_data_at_all=False,
                     has_omim_entries=False,
                     has_reportable_snpeff_entries=False,
                     has_reportable_vep_entries=False,
                     ):

        self.variants_count += 1

        variant = {
            'id': self.variants_count,
            'sample_id': self.sample_id,
            'reportable_clinvar_variations': [],
            'reportable_clinvar_entries': [],
            'reportable_gwas_associations': [],

            'all_clinvar_variations': [{}], # Has one ClinVar variation

            'omim_entries': [],
            'reportable_snpeff_annotations': [],
            'reportable_vep_annotations': [],
        }

        fake_entry = {}

        if has_reportable_clinvar_variations:
            variant['reportable_clinvar_variations'].append(fake_entry)

        if has_reportable_clinvar_entries:
            variant['reportable_clinvar_entries'].append(fake_entry)

        if has_reportable_gwas_entries:
            variant['reportable_gwas_associations'].append(fake_entry)

        if has_no_clinvar_data_at_all:
            variant['all_clinvar_variations'].clear()

        if has_omim_entries:
            variant['omim_entries'].append(fake_entry)

        if has_reportable_snpeff_entries:
            variant['reportable_snpeff_annotations'].append(fake_entry)

        if has_reportable_vep_entries:
            variant['reportable_vep_annotations'].append(fake_entry)

        return variant
