from reports_generation.pipeline import parse_frequencies_from_INFO


def test_parse_frequencies_from_INFO():
    variant = {
        'vcf_info': {
            'AC': '100',
            'AC_afr': '20',
            'AF': '1.5e-03',
            'AF_afr': '4.5e-04',
            'AF_eas': '3.0e-04',
            'non_topmed_popmax': 'eas',
            'other_field': 'foobar',
        },
        'vcf_ref': 'A',
        'vcf_alt': ['G'],
    }
    expected_result = {
        'G': {
            'AF': 0.0015,
            'AF_afr': 0.00045,
            'AF_eas': 0.0003,
        },
        'A': {
            'AF': 0.9985, # 1 - 0.0015
            'AF_afr': 0.99955, # 1 - 0.00045
            'AF_eas': 0.9997, # 1 - 0.0003
        },
    }
    result = parse_frequencies_from_INFO(variant)
    assert result == expected_result

    multiallelic_variant = {
        'vcf_info': {
            'AF': '1e-03,2e-03',
        },
        'vcf_ref': 'A',
        'vcf_alt': ['G', 'C'],
    }
    expected_result = {
        'G': {'AF': 0.001},
        'C': {'AF': 0.002},
        'A': {'AF': 0.997},
    }
    result = parse_frequencies_from_INFO(multiallelic_variant)
    assert result == expected_result

    multiallelic_with_missing = {
        'vcf_info': {
            'AF': '1e-03,.',
        },
        'vcf_ref': 'A',
        'vcf_alt': ['G', 'C'],
    }
    expected_result = {
        'G': {'AF': 0.001},
        'C': {'AF': 0},
        'A': {'AF': 0.999},
    }
    result = parse_frequencies_from_INFO(multiallelic_with_missing)
    assert result == expected_result

    missing_values_info = {
        'vcf_info': {},
        'vcf_ref': 'A',
        'vcf_alt': ['G']
    }
    result = parse_frequencies_from_INFO(missing_values_info)
    assert result == {}
