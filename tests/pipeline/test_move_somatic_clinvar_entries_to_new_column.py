from reports_generation.pipeline import move_somatic_clinvar_entries_to_new_column


def test_move_somatic_clinvar_entries_to_new_column():
    row = {
        'all_clinvar_entries': [
            {'origin': 'germline'},
            {'origin': None},
            {},
            {'origin': 'somatic'}
        ]
    }

    new_row = move_somatic_clinvar_entries_to_new_column(row)
    assert new_row['all_clinvar_entries'] == [{'origin': 'germline'}, {'origin': None}, {}]
    assert new_row['somatic_clinvar_entries'] == [{'origin': 'somatic'}]
