from copy import deepcopy

from reports_generation.pipeline import pick_reportable_entries


def test_pick_reportable_entries():
    gwas_assoc = {}
    snpeff_high = {'putative_impact': 'HIGH'}
    snpeff_low = {'putative_impact': 'LOW'}
    snpeff_missing_impact = {'putative_impact': None}
    clinvar_pathogenic = {'clinical_significances': ['Pathogenic', 'Pathogenic']}
    clinvar_benign = {'clinical_significances': ['Benign']}
    clinvar_variation_pathogenic = {'clinical_significances': ['Pathogenic']}
    clinvar_variation_benign = {'clinical_significances': ['Benign', 'Likely Benign']}
    vep_high = {'impact': 'HIGH'}
    vep_low = {'impact': 'LOW'}
    vep_missing_impact = {'impact': None}

    variant = {
        'sample_clinvar_variations': [clinvar_variation_pathogenic,
                                      clinvar_variation_benign],
        'sample_clinvar_entries': [clinvar_pathogenic, clinvar_benign],
        'sample_gwas_associations': [gwas_assoc],
        'sample_snpeff_annotations': [snpeff_high, snpeff_low,
                                      snpeff_missing_impact],
        'sample_vep_annotations': [vep_high, vep_low, vep_missing_impact],
    }

    parsed_variant = pick_reportable_entries(variant,
                                             min_reportable_category='HIGH')

    assert parsed_variant['reportable_clinvar_variations'] == [clinvar_variation_pathogenic]
    assert parsed_variant['reportable_clinvar_entries'] == [clinvar_pathogenic]
    assert parsed_variant['reportable_snpeff_annotations'] == [snpeff_high]
    assert parsed_variant['reportable_vep_annotations'] == [vep_high]
    assert parsed_variant['reportable_gwas_associations'] == [gwas_assoc]

    parsed_variant = pick_reportable_entries(variant,
                                             min_reportable_category='LOW')

    assert gwas_assoc in parsed_variant['reportable_gwas_associations']
    assert snpeff_high in parsed_variant['reportable_snpeff_annotations']
    assert snpeff_low in parsed_variant['reportable_snpeff_annotations']
    assert vep_high in parsed_variant['reportable_vep_annotations']
    assert vep_low in parsed_variant['reportable_vep_annotations']
    assert clinvar_pathogenic in parsed_variant['reportable_clinvar_entries']
    assert clinvar_benign not in parsed_variant['reportable_clinvar_entries']

    variant = {
        'sample_clinvar_variations': [clinvar_variation_benign],
        'sample_clinvar_entries': [clinvar_benign],
        'sample_gwas_associations': [gwas_assoc],
        'sample_snpeff_annotations': [snpeff_low],
        'sample_vep_annotations': [vep_low],
    }

    parsed_variant = pick_reportable_entries(deepcopy(variant),
                                             min_reportable_category='ASSOC')

    assert gwas_assoc in parsed_variant['reportable_gwas_associations']
    assert clinvar_variation_benign not in parsed_variant['reportable_clinvar_variations']
    assert clinvar_benign not in parsed_variant['reportable_clinvar_entries']
    assert snpeff_low not in parsed_variant['reportable_snpeff_annotations']
    assert vep_low not in parsed_variant['reportable_vep_annotations']

    parsed_variant = \
        pick_reportable_entries(deepcopy(variant),
                                min_reportable_category='PAT')
    assert gwas_assoc not in parsed_variant['reportable_gwas_associations']

    # Clinvar Variations is checked, not Clinvar entries, for GWAS associations
    # inclusion:
    variant = {
        'sample_clinvar_variations': [clinvar_variation_benign],
        'sample_clinvar_entries': [clinvar_pathogenic],
        'sample_gwas_associations': [gwas_assoc],
        'sample_snpeff_annotations': [],
        'sample_vep_annotations': [],
    }
    parsed_variant = pick_reportable_entries(deepcopy(variant),
                                             min_reportable_category='PAT')
    assert parsed_variant['reportable_gwas_associations'] == []

    variant = {
        'sample_clinvar_variations': [clinvar_variation_pathogenic],
        'sample_clinvar_entries': [clinvar_benign],
        'sample_gwas_associations': [gwas_assoc],
        'sample_snpeff_annotations': [],
        'sample_vep_annotations': [],
    }
    parsed_variant = pick_reportable_entries(deepcopy(variant),
                                             min_reportable_category='PAT')
    assert parsed_variant['reportable_gwas_associations'] == [gwas_assoc]
