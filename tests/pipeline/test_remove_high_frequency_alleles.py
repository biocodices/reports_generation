from reports_generation.pipeline import remove_high_frequency_alleles


THRESHOLD = 0.1


def test_remove_high_frequency_alleles():
    frequencies = {
        'A': {'dbSNP': {'General': 0.2}, 'CADD_1000g': {'African': 0.1}},
        'G': {'dbSNP': {'General': 0.9}},

        # No frequency data about the 'T' allele
    }
    entries_to_keep = [{'genomic_allele': 'A'}, {'genomic_allele': 'T'}]
    entry_to_lose = {'genomic_allele': 'G'}
    all_entries = entries_to_keep + [entry_to_lose]

    variant = {'frequencies': frequencies}
    fields = [
        'reportable_clinvar_entries',
        'reportable_clinvar_variations',
        'reportable_gwas_associations',
        'reportable_snpeff_annotations',
        'reportable_vep_annotations',
    ]
    for field in fields:
        variant[field] = all_entries

    resulting_variant = remove_high_frequency_alleles(variant,
                                                      max_frequency=THRESHOLD)

    for field in fields:
        print(field)
        assert entry_to_lose not in resulting_variant[field]

        for entry in entries_to_keep:
            assert entry in resulting_variant[field]

    # Test it keeps all entries if there's no frequency data
    variant['frequencies'] = None
    resulting_variant = remove_high_frequency_alleles(variant,
                                                      max_frequency=THRESHOLD)
    for field in fields:
        assert resulting_variant[field] == all_entries

def test_remove_high_frequency_alleles_from_ExAC():
    frequencies = { 'A': {'dbNSFP_ExAC': {'General': 0.2}} }
    variant = {'frequencies': frequencies,
               'reportable_clinvar_entries': [{'genomic_allele': 'A'}]}
    result = remove_high_frequency_alleles(variant, max_frequency=THRESHOLD)
    assert result['reportable_clinvar_entries'] == []
