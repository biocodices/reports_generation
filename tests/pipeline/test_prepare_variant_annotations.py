import pandas as pd

from reports_generation.pipeline import prepare_variant_annotations


def test_prepare_variant_annotations():
    annotations = pd.DataFrame([
        {
            'id': 'rs1',
            'ref': 'T',
            'alt': ['A'],
            'chrom': '1',
            'pos': '1000',
            'ensembl': {},
            'info': {},
            'frequencies': {},
            'entrez_gene_symbols': [],
            'dbsnp_myvariant': None,
            'clinvar_variations': None,
            'clinvar_entries': [
                {'origin': 'germline',
                 'clinical_significances': ['Pathogenic'],
                 'genomic_allele': 'A'},
                {'origin': 'somatic',
                 'clinical_significances': ['Pathogenic'],
                 'genomic_allele': 'A'},
            ],
            'clinvar_vcf_entries': None,
            'omim_entries': None,
            'snpeff_myvariant': None,
            'uniprot_entries': None,

            'gwas_catalog': [
                {
                    'rsids': ['rs1', 'rs2'],
                    'genomic_alleles': {'rs1': 'A', 'rs2': 'G'},
                }
            ],

            'dbsnp_web': None,
            'dbnsfp': None,
            'hgvs': None,
            'maf': None,
        }
    ])

    vep_annotations = pd.Series({
        'rs1': [{
            'uploaded_variation': 'rs1',
            'location': '1:1000',
            'impact': 'HIGH',
            'genomic_allele': 'A',
            'strand': 1.0,  # Strand is read as float sometimes!
            'hgvsc': 'c-change-1',
            'hgvsp': 'p-change-1',
            'symbol': 'GENE-1',
        }],
    })
    vep_annotations = (vep_annotations.to_frame().reset_index()
                       .rename(columns={'index': 'id_or_location', 0: 'vep_annotations'}))

    prepared_annotations = prepare_variant_annotations(annotations,
                                                       vep_annotations)

    new_columns_expected = [
        'is_REV',
        'dbsnp_url',
        'cds_strand',
        'risk_alleles',
        'pmids',
        'gmaf',
        'all_gwas_associations',
        'is_incidental',
        'clinvar_comments',
    ]

    for expected_column in new_columns_expected:
        assert expected_column not in annotations.columns
        assert expected_column in prepared_annotations.columns

    columns_that_should_be_all_lists = [
        'all_clinvar_variations',
        'all_clinvar_entries',
        'all_gwas_associations',
        'all_vep_annotations',
        'all_snpeff_annotations',
        'omim_entries',
        'uniprot_entries',
    ]

    for column in columns_that_should_be_all_lists:
        assert all(isinstance(entries, list)
                   for entries in prepared_annotations[column])

    # Test specific data were added during the parsing
    rs_variant = prepared_annotations.iloc[0]
    assert not rs_variant['is_REV']
    assert rs_variant['dbsnp_url'].startswith('http')
    assert rs_variant['cds_strand'] == '+'
    assert rs_variant['id_or_location'] == rs_variant['id']
    assert rs_variant['risk_alleles'] == ['A']
    assert not rs_variant['is_incidental']
    assert rs_variant['all_vep_annotations'][0]['impact'] == 'HIGH'
    assert rs_variant['entrez_gene_symbols'] == ['GENE-1']
    assert rs_variant['all_gwas_associations'][0]['genomic_allele'] == 'A'

    for clinvar_entry in rs_variant['all_clinvar_entries']:
        assert clinvar_entry['origin'] != 'somatic'

    assert len(rs_variant['somatic_clinvar_entries']) == 1
    assert len(rs_variant['all_clinvar_entries']) == 1
