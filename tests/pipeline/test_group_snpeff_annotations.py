from reports_generation.pipeline import group_snpeff_annotations


def test_group_snpeff_annotations():
    annotations = [
        # Intergenic alleles:
        # - High
        {'transcript_biotype': 'intergenic',
         'gene_id': 'Gene-1',
         'effects': ['eff-1'],
         'putative_impact': 'HIGH'},
        {'transcript_biotype': 'intergenic',
         'effects': ['eff-1'],
         'gene_id': 'Gene-1',
         'putative_impact': 'HIGH'},

        # - Low
        {'transcript_biotype': 'intergenic',
         'effects': ['eff-1'],
         'gene_id': 'Gene-1',
         'putative_impact': 'LOW'},

        # Coding alleles:
        # - Ala1Gly
        # -- High
        {'transcript_biotype': 'Coding',
         'hgvs_c': 'c.1A>T',
         'hgvs_p': 'p.Ala1Gly',
         'effects': ['eff-2'],
         'gene_id': 'Gene-1',
         'putative_impact': 'HIGH'},

        # -- Low
        {'transcript_biotype': 'Coding',
         'hgvs_c': 'c.1A>T',
         'hgvs_p': 'p.Ala1Gly',
         'effects': ['eff-2'],
         'gene_id': 'Gene-1',
         'putative_impact': 'LOW'},

        # - Ala1Met
        {'transcript_biotype': 'Coding',
         'hgvs_c': 'c.1A>G',
         'hgvs_p': 'p.Ala1Met',
         'effects': ['eff-3', 'eff-4'],
         'gene_id': 'Gene-1',
         'putative_impact': 'LOW'},

        # - With no hgvs p
        {'transcript_biotype': 'protein_coding',
         'hgvs_c': 'c.1A>G',
         'effects': ['eff-5'],
         'gene_id': 'Gene-1',
         'putative_impact': 'LOW'},
    ]

    grouped = group_snpeff_annotations(annotations)

    # Test grouping by the correct combination of fields:
    intergenic = 'intergenic:Gene-1:::eff-1'
    coding_gly = 'Coding:Gene-1:c.1A>T:p.Ala1Gly:eff-2'
    coding_met = 'Coding:Gene-1:c.1A>G:p.Ala1Met:eff-3-eff-4'
    no_hgvsp = 'protein_coding:Gene-1:c.1A>G::eff-5'

    assert set(grouped.keys()) == {intergenic, coding_gly, coding_met, no_hgvsp}
    assert len(grouped[intergenic]) == 2
    assert len(grouped[coding_gly]) == 2
    assert len(grouped[coding_met]) == 1
    assert len(grouped[no_hgvsp]) == 1

    # Test sub-grouping by impact
    assert set(grouped[coding_gly].keys()) == {'HIGH', 'LOW'}
    assert grouped[coding_met]['LOW'][0]['hgvs_p'] == 'p.Ala1Met'
