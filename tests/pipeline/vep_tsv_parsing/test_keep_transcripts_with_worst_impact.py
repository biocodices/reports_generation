import pandas as pd

from reports_generation.pipeline.vep_tsv_parsing import keep_transcripts_with_worst_impact


def test_keep_transcripts_with_worst_impact():
    df = pd.DataFrame([
        {'id': 1, 'uploaded_variation': '1', 'impact': 'LOW'},
        {'id': 2, 'uploaded_variation': '1', 'impact': 'MODERATE'},
        {'id': 3, 'uploaded_variation': '1', 'impact': 'MODERATE'},
        {'id': 4, 'uploaded_variation': '2', 'impact': 'LOW'},
        {'id': 5, 'uploaded_variation': '2', 'impact': 'HIGH'},
    ])
    result = keep_transcripts_with_worst_impact(df)
    assert list(result['id']) == [2, 3, 5]

