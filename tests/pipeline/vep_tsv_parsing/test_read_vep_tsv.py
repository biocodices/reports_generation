import pytest

from reports_generation.pipeline.vep_tsv_parsing import read_vep_tsv


def test_read_vep_tsv():
    vep_tsv = pytest.helpers.file('vep.tsv')
    result = read_vep_tsv(vep_tsv)

    # Typical VEP TSV has many entries per variant, so I reproduced that
    # in the test file:

    assert list(result['uploaded_variation']) == (
        ['rs201654872'] * 4 + \
        ['rs202102042'] * 4 + \
        ['1_1000_C/T'] * 4 + \
        ['2_2000_G/A'] * 4 + \
        ['3_3000_GG/-'] + \
        ['4_4000_T/-']
    )

