import pytest

from reports_generation.pipeline.vep_tsv_parsing import parse_vep_tsv
from reports_generation.pipeline.vep_tsv_parsing.parse_vep_tsv \
        import _parse_vep_indel_location


@pytest.mark.parametrize('vep_location,expected_output', [
    ('1:100', '1:99'),
    ('1:100-120', '1:99')
])
def test_parse_vep_indel_location(vep_location, expected_output):
    assert _parse_vep_indel_location(vep_location) == expected_output


def test_parse_vep_tsv():
    vep_tsv = pytest.helpers.file('vep.tsv')
    result = parse_vep_tsv(vep_tsv)

    expected_ids = [
        '1:1000',
        '2:2000',
        '3:2999',
        '4:3999',
        'rs201654872',
        'rs202102042',
    ]
    assert result.shape == (18, 15)
    assert sorted(result['id_or_location'].unique()) == expected_ids

    result = parse_vep_tsv(vep_tsv, as_dictionary=True)

    assert len(result) == 6
    assert sorted(result.keys()) == expected_ids
    assert all(isinstance(value, list) for value in result.values())

    result = parse_vep_tsv(vep_tsv, collapsed=True)

    assert list(result.columns) == ['id_or_location', 'vep_annotations']
    assert list(result['id_or_location']) == expected_ids
