import pandas as pd

from reports_generation.pipeline.vep_tsv_parsing import keep_NCBI_features


def test_keep_NCBI_features():
    df = pd.DataFrame([
        {'id': 1, 'uploaded_variation': 1,
         'gene': 'ENSG01', 'feature': 'ENST01'},
        {'id': 2, 'uploaded_variation': 1,
         'gene': '123', 'feature': 'NM_1'},
        {'id': 3, 'uploaded_variation': 1,
         'gene': 'ENSG01', 'feature': 'NM_1'},
        {'id': 4, 'uploaded_variation': 1,
         'gene': '234', 'feature': 'ENST01'},
    ])
    result = keep_NCBI_features(df)
    assert list(result['id']) == [2]

