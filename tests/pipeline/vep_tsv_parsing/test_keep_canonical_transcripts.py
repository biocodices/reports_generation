import pytest
import pandas as pd

from reports_generation.pipeline.vep_tsv_parsing import keep_canonical_transcripts


def test_keep_canonical_transcripts():
    df = pd.DataFrame([
        {'id': 1, 'uploaded_variation': 1, 'canonical': 'YES'},
        {'id': 2, 'uploaded_variation': 1, 'canonical': '-'},
        {'id': 3, 'uploaded_variation': 2, 'canonical': 'YES'},
        {'id': 4, 'uploaded_variation': 2, 'canonical': '-'},
    ])
    result = keep_canonical_transcripts(df)
    assert list(result['id']) == [1, 3]

    df = df.append(
        # A variant with no canonical transcript
        {'id': 5, 'uploaded_variation': 3, 'canonical': '-'},
        ignore_index=True
    )

    with pytest.raises(Exception):
        result = keep_canonical_transcripts(df)

