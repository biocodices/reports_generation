import pandas as pd

from reports_generation.pipeline import extract_pmids


def test_extract_pmids():
    variant = pd.Series({
        'omim_entries': [
            {'pubmed_entries': [{'pmid': '1'}]},
            {'pubmed_entries': [{'pmid': '1'},
                                {'pmid': '2'},
                                {'pmid': '3'}]},
            {'pubmed_entries': [{'pmid': None},
                                {}]}
        ],
        'all_gwas_associations': []
    })

    assert extract_pmids(variant) == ['1', '2', '3']

    variant['all_gwas_associations'] = [
        {'pubmed_entries': [{'pmid': '1'}]},  # Dupe one
        {'pubmed_entries': [{'pmid': '4'}]},  # New one
    ]

    assert extract_pmids(variant) == ['1', '2', '3', '4']
