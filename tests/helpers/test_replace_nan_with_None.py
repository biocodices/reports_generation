import pandas as pd
import numpy as np

from reports_generation.helpers import replace_nan_with_None


def test_replace_nan_with_None():
    df = pd.DataFrame.from_dict(data={
        'strings': ['1', np.nan],
        'lists': [[], np.nan],
        'dicts': [{}, np.nan],
    })
    result = replace_nan_with_None(df)
    nan_entry = result.iloc[1]
    assert nan_entry['strings'] is None
    assert nan_entry['lists'] is None
    assert nan_entry['dicts'] is None

