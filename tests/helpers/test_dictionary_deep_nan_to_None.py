import json

import pytest
import pandas as pd

from reports_generation.helpers import dictionary_deep_nan_to_None


def test_dictionary_deep_nan_to_None():
    nan = pd.Series([0.1, None]).iloc[1]
    dictionary_with_deep_nans = {
        "level-1": nan,
        "in-list": [nan],
        "list-of-dicts": [{"foo": nan}],
        "in-dict": {
            "level-2": nan,
            "in-list": [nan],
            "in-dict": {
                "level-3": nan
            }
        }
    }

    parsed_dictionary = dictionary_deep_nan_to_None(dictionary_with_deep_nans)

    with pytest.raises(ValueError):
        json.dumps(dictionary_with_deep_nans, allow_nan=False)

    # Should not break
    json.dumps(parsed_dictionary, allow_nan=False)
