from reports_generation.helpers import extract_key_of_all_dicts


def test_extract_key_of_all_dicts():
    list_of_dicts = [
        {'foo': 'bar'},
        {'foo': 'baz'},
        {'foo': 'baz'},
    ]

    result = extract_key_of_all_dicts(list_of_dicts, key='foo')
    assert result == ['bar', 'baz']

    result = extract_key_of_all_dicts(list_of_dicts, key='foo', count=True)
    assert result == {'bar': 1, 'baz': 2}

