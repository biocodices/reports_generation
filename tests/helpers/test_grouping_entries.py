import pytest

from reports_generation.helpers import (
    key_from_fields,
    group_annotations_by_fields,
    sort_groups_by_worst_category,
)


def test_key_from_fields():
    annotation = {
        'key_1': '1',
        'key_2': '2',
        'key_3': ['3', '4'],
    }

    # Test that only the wanted keys are used, and in the passed order:
    assert key_from_fields(annotation, ['key_1', 'key_2']) == '1:2'

    # Test it doesn't break if some keys are not present
    assert key_from_fields(annotation, ['key_1', 'non_key', 'key_2']) == '1::2'

    # Test it joins nested lists
    assert key_from_fields(annotation, ['key_2', 'key_3']) == '2:3-4'

    # Test it breaks if no keys are present
    with pytest.raises(KeyError):
        assert key_from_fields(annotation, ['non_key'])


def test_group_annotations_by_fields():
    annotations = [
        # These should be grouped together because of the same key_1 and key_2
        # values
        {'key_1': 'foo', 'key_2': 'baz', 'key_3': 'spam'},
        {'key_1': 'foo', 'key_2': 'baz', 'key_3': 'eggs'},

        # Entries with one missing value. They should be grouped together
        # because of the same key_1 value. key_3 is different, but it should be
        # ignored.
        {'key_1': 'bar', 'key_3': 'qux'},
        {'key_1': 'bar', 'key_3': 'boo'},
    ]

    grouped = group_annotations_by_fields(annotations,
                                          fields=['key_1', 'key_2'])

    assert len(grouped) == 2
    assert grouped['foo:baz'] == annotations[:2]
    assert grouped['bar:'] == annotations[2:]


def test_sort_groups_by_worst_category():
    grouped_annotations = {
        'mid-entry': {
            'Likely pathogenic': [],
            'LOW': [],
        },
        'worst-entry': {
            'Pathogenic': [],
            'LOW': [],
        },
        'best-entry': {
            'Benign': [],
            'HIGH': [],
        }
    }

    sorted_annotations = sort_groups_by_worst_category(grouped_annotations)
    keys = list(sorted_annotations.keys())

    # Test the order of the entries!
    assert keys == ['worst-entry', 'mid-entry', 'best-entry']
