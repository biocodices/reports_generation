import pytest
import pandas as pd
import numpy as np

from reports_generation.helpers import (
    replace_nan_with_empty_list,
    _replace_nan_with_empty_list,
)


@pytest.mark.parametrize('items', [
    [['foo'], [], None],  # object dtype
    [1, 2, None],  # float dtype
])
def test__replace_nan_with_empty_list(items):
    series = pd.Series(items)
    new_series = _replace_nan_with_empty_list(series)
    assert new_series.iloc[2] == []


def test_replace_nan_with_empty_list():
    null_data = {'field-name': np.nan}
    null_data.update({
        'id': 1,
        'hgvs': None,  # Check it handles None besides NaN
        'other-field': np.nan,
    })

    nonnull_data = {'field-name': ['foo']}
    nonnull_data.update({'id': 2})

    df = pd.DataFrame([null_data, nonnull_data])

    replace_nan_with_empty_list(df, columns=['field-name'])

    entry_1 = df.set_index('id').loc[1]
    entry_2 = df.set_index('id').loc[2]

    assert entry_1['field-name'] == []  # An empty list was put in place of NaN
    assert entry_2['field-name'] == ['foo']  # Wasn't modified

    # Check it doesn't affect other fields
    assert pd.isnull(entry_1['other-field'])

    # Check it tolerates any missing columns
    replace_nan_with_empty_list(pd.DataFrame({}), columns=['field-name'])
