import pandas as pd
import numpy as np

from reports_generation.helpers import is_nan


def test_is_nan():
    series = pd.Series([1, None])

    assert not is_nan(series.iloc[0])
    assert is_nan(series.iloc[1])

    # Check it doesn't break with lists/arrays
    series = pd.Series([1, [1, 2, 3], [], [np.nan]])

    assert not is_nan(series.iloc[0])
    assert not is_nan(series.iloc[1])

    # Lists are never NaN, no matter their contents
    assert not is_nan(series.iloc[2])
    assert not is_nan(series.iloc[3])

