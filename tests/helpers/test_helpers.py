from functools import partial

import pytest

from reports_generation.helpers import (
    check_samples_in_vcf,
    reverse_complement,
)


def test_check_samples_in_vcf():
    f = partial(check_samples_in_vcf, vcf=pytest.helpers.file('genotypes.vcf'))

    assert f(samples='HG00096') == ['HG00096']
    assert f(samples=['HG00096', 'HG00097']) == ['HG00096', 'HG00097']

    with pytest.raises(ValueError):
        assert f(samples='Unavailable-Sample')


def test_reverse_complement():
    assert reverse_complement('A') == 'T'

