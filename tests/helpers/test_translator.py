import pytest

from reports_generation.helpers import Translator


def test_translator_load_translations():
    translations_dir = pytest.helpers.file('test_translations')
    translations = Translator._load_translations(translations_dir)
    assert 'key_in_default_yml' in translations  # Default is loaded at level 0
    assert translations['root_level'] == {'foo': 'bar'}  # Foo -> foo
    assert translations['test'] == {'foo': 'foo', 'bar': 'bar',
                                    'baz': {'qux': {'spam': 'eggs'}}}

    with pytest.raises(Exception):
        Translator._load_translations('/tmp/nonexistentdir')


def test_translator_functionality():
    translations_dir = pytest.helpers.file('test_translations')
    translator = Translator(translations_dir)
    assert translator.ugettext('test:foo') == 'foo'
    assert translator.ugettext('test:baz:qux:spam') == 'eggs'

    # In default.yml you don't need to prepend the namespace to access
    # to the keys:
    assert translator.ugettext('key_in_default_yml') == 'value_in_default_yml'

    translations = {'test': {'foo': 'bar'}}
    assert translator._deep_search('test:foo', translations) == 'bar'

    translator.ugettext('non:existent:key')  # Will log.warning the failure
    assert 'non:existent:key' in translator.untranslated

    # Gets the value from test:foo_plural
    assert translator.ngettext('plurals:irregular_bar', 0) == 'barz'
    assert translator.ngettext('plurals:irregular_bar', 1) == 'bar'
    assert translator.ngettext('plurals:irregular_bar', 2) == 'barz'

    # Builds the -s plural from the singular form:
    assert translator.ngettext('test:foo', 2) == 'foos'

    # Builds the -es plural from the singular form:
    assert translator.ngettext('test:bar', 2) == 'bares'

