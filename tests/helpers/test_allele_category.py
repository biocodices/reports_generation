import pytest

from reports_generation.helpers import AlleleCategory


def test_allele_category():
    assert AlleleCategory('drug response') == AlleleCategory('DRUG')
    assert AlleleCategory('drUg RESPONse') == AlleleCategory('DRUG')
    assert AlleleCategory('MODERATE') != AlleleCategory('ASSOC')
    assert AlleleCategory('not provided') < AlleleCategory('MODIFIER')
    assert max(AlleleCategory('HIGH'), AlleleCategory('LOW')).tag == 'HIGH'
    assert AlleleCategory('PAT').description == 'Pathogenic'

    with pytest.raises(ValueError):
        assert AlleleCategory(None)

    with pytest.raises(ValueError):
        AlleleCategory('non_existent_category')


def test_get_category_rank():
    assert AlleleCategory.get_category_rank('PAT') == 17

