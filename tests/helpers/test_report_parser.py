from unittest.mock import Mock

from bs4 import BeautifulSoup
import pytest

from reports_generation.helpers import ReportParser


def test_init():
    html_file = pytest.helpers.file('report.html')
    parser = ReportParser(html_file)
    assert parser.html == '<html>Some content</html>\n'

    with pytest.raises(FileNotFoundError):
        html_file = pytest.helpers.file('non-existent-file')
        ReportParser(html_file)


def test_make_soup():
    html = """
    <html>
        <div class="class-1">Some text</div>
    </html>
    """
    soup = ReportParser._make_soup(html)
    assert soup.select_one('.class-1').text == 'Some text'


def test_extract_variants_from_soup():
    html = """
    <div class="variant-detail"></div>
    <div class="variant-detail"></div>
    <div class="variant-detail non-reportable"></div>
    """
    soup = BeautifulSoup(html, 'lxml')

    reportable_variants = ReportParser._extract_variants_from_soup(soup)
    assert len(reportable_variants) == 2

    all_variants = ReportParser._extract_variants_from_soup(
        soup,
        include_nonreportable=True
    )
    assert len(all_variants) == 3


def test_extract_variant_info():
    variant_html = """
    <div class="variant-detail">
        <span data-key="Key-1">
            Value-1a
            <br>
            <!-- Some whitespace without info -->
            Value-1b
            <br>
        </span>

        <span data-key="Key-2" data-value="Value-2a">
            This text doesn't matter because "data-value" attribute is present.
        </span>

        <span data-key="Key-2">
            Value-2b
        </span>

        <p class="some-class non-reportable">
            <span>
                <span data-key="Key-3">
                    Value-3
                </span>
            </span>
        </p>

        <span data-key="Key-4"></span>
        <span data-key="Key-4"></span>
    </div>
    """
    variant_soup = BeautifulSoup(variant_html, 'lxml')
    info = ReportParser._extract_variant_info(variant_soup)

    # Whitespace should be properly cleaned:
    assert info['Key-1'] == 'Value-1a Value-1b'

    # If the key is seen multiple times, the value should be a list:
    assert info['Key-2'] == ['Value-2a', 'Value-2b']

    # If the key is under a .non-reportable element (at any parent level)
    # it shouldn't be read:
    assert 'Key-3' not in info

    # Empty values are considered
    assert info['Key-4'] == ['n/a', 'n/a']


def test_extract_title_from_soup():
    html = """
    <div class="main-title">
        <h1>
            Report Title
        </h1>
    </div>
    """
    soup = BeautifulSoup(html, 'lxml')
    title = ReportParser._extract_title_from_soup(soup)
    assert title == 'Report Title'


def test_parse_variants_html(monkeypatch):
    def mock_extract_info(self, variant):
        mock_extract_info.counter += 1
        return {
            'variant_rank_number': mock_extract_info.counter,
            'more_info': 'foo-{}'.format(mock_extract_info.counter),
        }
    mock_extract_info.counter = 0

    monkeypatch.setattr(ReportParser, '_extract_variant_info',
                        mock_extract_info)
    monkeypatch.setattr(ReportParser, '_read_report_file', Mock())

    parser = ReportParser('/path/to/report')
    info = parser._parse_variants_html(['variant-1-html',
                                        'variant-2-html',
                                        'variant-3-html'])

    assert list(info.keys()) == [1, 2, 3]
    assert info[1]['more_info'] == 'foo-1'


def test_parse_the_report(monkeypatch):
    instance = Mock(
        html='html',
        _make_soup=Mock(return_value='soup'),
        _extract_variants_from_soup=Mock(),
        _extract_title_from_soup=Mock(return_value='title'),
        _parse_variants_html=Mock(return_value={'Var-1': {'foo': 'bar'}}),
        _info_dict_as_dataframe=Mock(return_value='dataframe_of_variants'),
    )

    result = ReportParser.parse_the_report(instance)

    assert instance._make_soup.call_args[0][0] == 'html'
    assert instance._extract_variants_from_soup.call_args[0][0] == 'soup'
    assert instance._extract_variants_from_soup.call_args[1] == \
        {'include_nonreportable': False}
    instance._parse_variants_html.assert_called_once()
    instance._info_dict_as_dataframe.assert_called_once()
    assert result == 'dataframe_of_variants'

    ReportParser.parse_the_report(instance, include_nonreportable=True)

    assert instance._extract_variants_from_soup.call_args[1] == \
        {'include_nonreportable': True}

    result = ReportParser.parse_the_report(instance, return_dataframe=False)

    assert result == {'Var-1': {'foo': 'bar', 'report_title': 'title'}}


def test_info_dict_as_dataframe():
    info_dict = {
        'Variant-1': {
            'String-Attribute': 'foo',
            'List-Attribute': ['foo', 'bar', 'foo'],  # Repeated item
            'gmaf': 0.1234321,
        },
        'Variant-2': {
            'String-Attribute': 'bar',
            'List-Attribute': ['bar', 'baz'],
            'Int-Attribute': 1,
        },
        'Variant-3': {
            'List-Attribute': None,
        }
    }
    result = ReportParser._info_dict_as_dataframe(info_dict)
    assert result.loc['Variant-1', 'String-Attribute'] == 'foo'
    assert result.loc['Variant-2', 'String-Attribute'] == 'bar'
    assert result.loc['Variant-1', 'List-Attribute'] == ['foo', 'bar']
    assert result.loc['Variant-2', 'List-Attribute'] == ['bar', 'baz']
    assert result.loc['Variant-1', 'gmaf'] == 0.1234
    assert result.loc['Variant-2', 'Int-Attribute'] == 1
    assert result.loc['Variant-3', 'List-Attribute'] == None

