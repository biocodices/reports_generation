import pytest

from reports_generation.helpers import is_SNP


@pytest.mark.parametrize('variant,expected_result', [
    # INFO 'VT' available:
    ({'vcf_info': {'VT': 'SNP'}, 'vcf_ref': 'A', 'vcf_alt': ['T']}, True),
    ({'vcf_info': {'VT': 'INDEL'}, 'vcf_ref': 'ATT', 'vcf_alt': ['A']}, False),

    # No info VT datum, infer from alleles:
    ({'vcf_info': {}, 'vcf_ref': 'A', 'vcf_alt': ['T']}, True),
    ({'vcf_info': {}, 'vcf_ref': 'A', 'vcf_alt': ['T', 'G']}, True),
    ({'vcf_info': {}, 'vcf_ref': 'ATT', 'vcf_alt': ['A']}, False),
    ({'vcf_info': {}, 'vcf_ref': 'A', 'vcf_alt': ['ATT']}, False),
    ({'vcf_info': {}, 'vcf_ref': 'A', 'vcf_alt': ['AA']}, False),
    ({'vcf_info': {}, 'vcf_ref': 'A', 'vcf_alt': ['<CN1>']}, False),
])
def test_is_SNP(variant, expected_result):
    assert is_SNP(variant) == expected_result

