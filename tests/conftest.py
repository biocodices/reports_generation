from os.path import join, dirname, basename

pytest_plugins = ['helpers_namespace']

import pytest


@pytest.helpers.register
def file(filename):
    tests_directory = dirname(__file__)

    while basename(tests_directory) != 'tests':
        tests_directory = dirname(tests_directory)

    return join(tests_directory, 'files/{}'.format(filename))

