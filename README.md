# Run from Python

```python
from reports_generation import ReportsPipeline

reports_pipeline = ReportsPipeline(
    # Single-sample VCF with the genotypes to report:
    vcf='path/to/vcf',

    # Annotated variants and genes as produced by `anotamela` AnnotationPipeline.
    # These annotations should be about the same IDs foudn in the VCF above:
    variants_json='path/to/variants.json',
    genes_json='path/to/genes.json',
    vep_tsv='path/to/vep.tsv',

    # Where to put the big results JSON:
    outdir='path/for/output',

    # Optional parameters to filter the variants to be reported:
    min_reportable_category='ASSOC', # See `helpers/allele_category.py` for a complete list
    min_odds_ratio=1.5, # E.g. Only report associations with high OR
    max_frequency=0.05, # E.g. Only report rare alleles
    phenos_regex_list=['Alzheimer', 'Parkinson'], # Remove phenotypes that don't match these

    # DEPRECATED: This was used to make HTML reports.
    # (Now we make PDF reports with a separate Rails app, `panels`)
    ## translations_dir='~/repos/reports_generation/reports_generation/translations/',
    ## templates_dir='~/repos/reports_generation/reports_generation/templates/',
)

reports_pipeline.run(samples='NA20509')
# ^ This sample ID must exactly match the one found in the VCF you provided above
```

# Run from `paip`

A luigi task was written in [`paip`](https://github.com/biocodices/paip) to
avoid writing the code above every time:

```bash
# Basic mode with default settings:
 paip GenerateReports --basedir .

# Run only for one sample and with a different category threshold
 paip GenerateReports --min-reportable-category RISKF --sample ONC-18-001 --workers 4
```

# Developers, developers, developers

## Test

```python
pytest -vv -x
```


## Regenerate test files

If you need to regenerate the `genes.json` and `variants.json` files under
`tests/files`, in case the code of `anotamela` changed, you can do so like this:

```python
from os.path import join, expanduser
from anotamela import AnnotationPipeline


# Replace with your path:
base_dir = expanduser('~/repos/reports_generation/tests/files')

# tor_hostname = DEFINE THIS!
proxies = {'http': f'socks5://{tor_hostname}:9050'}

pipe = AnnotationPipeline(cache='dict', proxies=proxies)

_ = pipe.run_from_vcf(join(base_dir, 'genotypes.vcf'))

pipe.rs_variants.to_json(join(base_dir, 'variants.json'), orient='split')
pipe.gene_annotations.to_json(join(base_dir, 'genes.json'), orient='split')
```

## General organization of the ReportsPipeline

Typically, you come to ReportsPipeline with four files from a previous
variant calling and annotation pipeline:

- A single-sample VCF with genotypes.
- A TSV of Variant Effect Predictor annotations for the variants in that VCF.
- Two JSON files result of `anotamela`'s `AnnotationPipeline`:
    - A JSON of annotations for the variants found in that VCF.
    - A JSON of annotations for the genes associated to those variants.

You initialize `ReportsPipeline` with those files and some pipeline settings,
and then run it to produce a `report_data_full.json`, which will be used by
`panels`, a `rails` app to generate a PDF report.

The pipeline to produce that JSON has many steps (method `run`):

- `prepare_variant_annotations`: Parse the annotations in ways that are
    sample-agnostic. We do not use the sample genotypes info here.
    The main goal is to parse some annotations and to reduce the lists
    of entries of ClinVar, GWAS, VEP, and SnpEff according to the
    thresholds passed during initialization.
- `prepare_gene_annotations`: Parse the gene annotations to make the relevant
    data easier to access.
- `_sample_pipeline`:
    - Filter the entries that are reportable because they are about
        alleles present in the sample.
    - Further parsing/sorting of the annotations according to relevance,
        now with the information of the sample genotypes.
    - Write the big JSON for the sample report.


There is a logic to the successive filtering of variants, which is reflected
in the naming of the lists of entries:

- `all_clinvar_entries` are all original ClinVar entries
    (analogously, `all_vep_annotations`, `all_gwas_assciations`, etc.)

- `reportable_clinvar_entries` are the ClinVar entries that are
    "reportable" **according to the chosen threshold of significance**, say
    if you chose "Likely Pathogenic", then the reportable entries will be
    the likely pathogenic and pathogenic ones, but not the bening ones.

    Thus, the `reportable` entries are a subset of `all` entries.

- `sample_clinvar_entries` are the _reportable_ entries which _also_ are about
    alleles **present in the sample**. An entry about allele G could be reportable
    because it's pathogenic, but if the sample has genotype T/T (i.e. no G 
    allele), then the entry will be remove.

    Thus, `sample` entries are a subset of the `reportable` entries and are the
    ones that make it into the final report.

The first version of this pipeline ended with HTML reports, but now PDF
reports are preferred and the `write_html_report` option is off by default,
although still functional.
