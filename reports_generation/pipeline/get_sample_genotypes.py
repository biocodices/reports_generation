from vcf_to_dataframe import vcf_to_dataframe

from reports_generation.helpers import replace_nan_with_None


def get_sample_genotypes(sample_id, vcf_path):
    """
    Get the genotypes for a single sample in the passed VCF file.
    The sample ID will be kept in genotypes.sample_id and the genotype
    annotations (like AD, DP, PL, etc.) will be added in separate columns.
    """
    genotypes = vcf_to_dataframe(vcf_path, keep_samples=[sample_id],
                                 keep_format_data=True)
    genotypes['sample_id'] = sample_id

    vcf_cols = 'id chrom pos ref alt qual filter info'.split()
    vcf_cols_new_names = {name: 'vcf_' + name for name in vcf_cols}
    genotypes = genotypes.rename(columns=vcf_cols_new_names)
    genotypes = genotypes.rename(columns={'GT': 'genotype',
                                          'GQ': 'genotype_quality',
                                          'PL': 'phred_likelihood',
                                          'AD': 'allele_depth',
                                          'DP': 'read_depth'})

    # Replace the NaN with None to ease downstream parsing.
    # This works for non-float data types (e.g. 'FT', 'allele_depth').
    # In numeric columns (e.g. 'read_depth', 'genotype_quality'),
    # NaN will be kept by pandas after the applymap anyway.
    genotypes = replace_nan_with_None(genotypes)

    return genotypes

