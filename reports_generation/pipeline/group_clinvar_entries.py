from itertools import groupby
from collections import OrderedDict

from reports_generation.helpers import (
    AlleleCategory,
    group_annotations_by_fields,
    sort_groups_by_worst_category,
)


def group_clinvar_entries(clinvar_entries):
    """
    Given a list of ClinVar entries for one SNP/indel, group them:

        - by their transcript/cds gene change/prot change
        - then by their clinical significance
        - and sort the transcripts by worst significance

    Returns a two-level OrderedDict. For instance:

        {
            # First transcript (pathogenic is the worst significance)
            'NM_123:c.1A>G:p.1Ala>Gly': {
                # The nested dict it also an OrderedDictionary:

                # First group: pathogenic entries for this transcript
                'Pathogenic': [{...}, {...}],

                # Second group: benign entries for the same transcript
                'Benign': [{...}, {...}],
            }

            # Second transcript (likely benging)
            'NM_234:c.1A>G:p.1Ala>Gly': {
                'Likely benign': [{...}],
            }
        }
    """
    if not clinvar_entries:
        return {}

    fields = 'transcript gene_symbol cds_change prot_change'.split()
    entries_by_transcript = group_annotations_by_fields(clinvar_entries, fields)

    grouped_entries = {}

    for transcript, transcript_entries in entries_by_transcript.items():
        if transcript not in grouped_entries:
            grouped_entries[transcript] = OrderedDict()

        entries_by_category = {k: list(v)
                               for k, v in groupby(transcript_entries,
                                                   key=entry_worst_significance)}

        sorted_categories = sorted(entries_by_category.keys(), reverse=True)

        for category in sorted_categories:
            grouped_entries[transcript][category.description] = \
                entries_by_category[category]

    return sort_groups_by_worst_category(grouped_entries)


def entry_worst_significance(entry):
    return max(AlleleCategory(sig) for sig in entry['clinical_significances'])
