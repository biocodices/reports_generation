import pandas as pd

from copy import deepcopy


def merge_frequencies(variant):
    """
    Given a variant with:

    - 'vcf_frequencies'
    {
      'A': { 'AF': 0.1, 'AF_afr': 0.15, ... },
      'G': { ... other VCF freqs ... }
    }
    - 'frequencies':
    {
      'A': {
        'Source': {'AFR': 0.2, ... }
      }
    }

    Return a dictionary with both vcf_frequencies and frequencies merged:

    {
      'A': {
        'Source': {'AFR': 0.2, ... }
        'VCF': { 'AF': 0.1, 'AF_afr': 0.15, ... }
      },
      'G': {
        'VCF': { .. other VCF freqs ... }
      }
    }
    """
    vcf_frequencies = variant.get('vcf_frequencies')
    if pd.isnull(vcf_frequencies):
        vcf_frequencies = {}

    other_frequencies = variant.get('frequencies') or {}
    if pd.isnull(other_frequencies):
        other_frequencies = {}

    # Strategy: merge the VCF frequencies INTO the other frequencies under the
    # key "VCF".
    all_frequencies = deepcopy(other_frequencies)

    for allele, vcf_allele_frequencies in vcf_frequencies.items():
        if allele not in all_frequencies:
            all_frequencies[allele] = {}
        all_frequencies[allele].update({'VCF': vcf_allele_frequencies})

    return all_frequencies
