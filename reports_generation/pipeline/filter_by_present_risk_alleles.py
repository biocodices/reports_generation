from reports_generation.helpers import is_SNP


def filter_by_present_risk_alleles(variant, annotations_key):
    """
    Input: a *variant* with a list of annotations under the given
    *annotations_key* and a list of alleles under the key
    'present_risk_alleles'.

    The function checks each of the annotations, which must refer to a
    'genomic_allele' (that should be a key of the dictionary), keeping only
    those annotations that refer to a genomic allele that is seen among the
    risk alleles present in the given sample. So, a ClinVar entry for
    'genomic_allele': 'G' will be kept if the present_risk_alleles include 'G',
    and that is because 'G' is both a risk allele and an allele present in the
    sample's genotype.

    It's important that this check is against *genomic* and not *coding*
    alleles, since coding alleles are the complement in the case of genes
    coding in the -1 strand. So a genomic change chr1:123A>G will have a
    coding sequence change expressed as T>C if it codes in the reverse strand.

    Filtering is not done if any alleles are not SNPs (keeps every annotation),
    or if we don't known the variant type.

    Returns a new list with the annotations that were kept in this way.
    """
    annotations = variant[annotations_key] or []

    if not is_SNP(variant):
        return annotations  # Don't filter anything for non-SNPs

    annotations_about_uncertain_alleles = []
    annotations_about_a_risk_allele = []

    for annotation in annotations:
        if not annotation.get('genomic_allele'):
            annotations_about_uncertain_alleles.append(annotation)
        elif annotation['genomic_allele'] in variant['present_risk_alleles']:
            annotations_about_a_risk_allele.append(annotation)

    # This includes annotations about uncertain alleles:
    return annotations_about_a_risk_allele + annotations_about_uncertain_alleles

    # This does NOT include annotations about uncertain alleles:
    # return annotations_about_a_risk_allele
