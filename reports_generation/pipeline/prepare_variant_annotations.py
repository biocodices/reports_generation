import logging

from reports_generation.pipeline import (
    add_VEP_genes,
    extract_cds_strand,
    infer_risk_alleles,
    is_incidental_finding,
    extract_gmaf,
    extract_pmids,
    unify_omim_phenotypes,
    make_dbsnp_url,
    parse_gwas_catalog_associations,
    remove_haplotype_associations,
    move_somatic_clinvar_entries_to_new_column,
    snp_alleles_reported_reverse,
    merge_variant_annotations,
    add_variation_info_to_clinvar_entries,
    extract_clinvar_comments,
)

from reports_generation.helpers import replace_nan_with_empty_list
from reports_generation.helpers import replace_nan_with_None


logger = logging.getLogger(__name__)


def prepare_variant_annotations(annotations,
                                vep_annotations=None):
    """
    Adds several extra fields to the passed variant *annotations* dataframe
    and combine those with the dictionary *vep_annotations*.

    It returns a new dataframe with the merged + extra annotations.
    """
    if vep_annotations is not None:
        df = merge_variant_annotations(annotations, vep_annotations)
        df = replace_nan_with_None(df)
    else:
        df = replace_nan_with_None(annotations)

    df['is_REV'] = df['dbsnp_web'].fillna(False).map(snp_alleles_reported_reverse)

    logger.info('Replace NaN with empty lists for easier parsing')
    df = replace_nan_with_empty_list(df, columns=[
        'dbsnp_myvariant',
        'clinvar_entries',
        'clinvar_vcf_entries',
        'clinvar_variations',
        'omim_entries',
        'uniprot_entries',
        'snpeff_myvariant',
        'vep_annotations',
        'gwas_catalog',
        'hgvs',
        'dbnsfp',
        'entrez_gene_symbols',
    ])

    df.rename(columns={
        'gwas_catalog': 'all_gwas_associations',
        'vep_annotations': 'all_vep_annotations',
        'clinvar_entries': 'all_clinvar_entries',
        'clinvar_variations': 'all_clinvar_variations',
        'snpeff_myvariant': 'all_snpeff_annotations',
    }, inplace=True)

    if vep_annotations is not None:
        logger.info('Add VEP gene symbols to the listed genes per variant')
        df['entrez_gene_symbols'] = df.apply(add_VEP_genes, axis=1,
                                             result_type='reduce')

    logger.info('Add dbSNP URLs')
    df['dbsnp_url'] = df['id'].map(make_dbsnp_url)

    logger.info('Extract coding strand')
    df['cds_strand'] = df.apply(extract_cds_strand, axis=1, result_type='reduce')

    logger.info('Unify OMIM phenotypes')
    df['omim_entries'] = df['omim_entries'].map(unify_omim_phenotypes)

    logger.info('Parse multi-SNP GWAS Catalog associations')
    df['all_gwas_associations'] = df.apply(parse_gwas_catalog_associations,
                                           axis=1, result_type='reduce')

    logger.info('Add ClinVar variation info to ClinVar entries')
    df['all_clinvar_entries'] = \
        df.apply(add_variation_info_to_clinvar_entries, axis=1)

    logger.info('Move somatic ClinVar entries to a different column')
    df = df.apply(move_somatic_clinvar_entries_to_new_column, axis=1)

    logger.info('Remove haplotype GWAS associations')
    df['all_gwas_associations'] = (df['all_gwas_associations']
                                   .map(remove_haplotype_associations))

    logger.info('List the "risk" alleles (i.e. alleles with reportable entries)')
    df['risk_alleles'] = df.apply(infer_risk_alleles, axis=1,
                                  result_type='reduce')

    logger.info('Extract PubMed IDs from GWAS and OMIM entries')
    df['pmids'] = df.apply(extract_pmids, axis=1, result_type='reduce')

    logger.info('Extract gMAF from Ensembl annotation')
    df['gmaf'] = df['ensembl'].map(extract_gmaf, na_action='ignore')

    logger.info('Identify incidental findings to be reported')
    df['is_incidental'] = df['omim_entries'].map(is_incidental_finding)

    logger.info('Extract ClinVar comments to a list')
    df['clinvar_comments'] = \
        df['all_clinvar_variations'].map(extract_clinvar_comments)

    return df
