import logging

import pandas as pd

logger = logging.getLogger(__name__)


def keep_NCBI_features(vep_df):
    """
    VEP annotations are duplicated: one row with Ensembl transcript/gene IDs
    and one with NCBI IDs. Keeps only the rows with NCBI IDs and returns a new
    dataframe.

    However, some annotations are only available for features with an Ensembl
    ID but not with an NCBI ID. For those, we keep the first available
    annotation.

    (This is the case of a handful of annotations only, and they seem to be
    intronic.)
    """
    ncbi_features = pd.DataFrame({})

    for variant_name, variant_annotations in vep_df.groupby('uploaded_variation'):
        # We keep the NCBI features by removing the Ensemble-ID ones.
        ens_gene = variant_annotations['gene'].str.startswith('ENS')
        ens_transcript = variant_annotations['feature'].str.startswith('ENS')
        annotations_to_keep = variant_annotations[~ens_gene & ~ens_transcript]

        if annotations_to_keep.empty:
            logger.warning('No NCBI features for {}. Keeping an Ensembl one.'
                           .format(variant_name))

        ncbi_features = ncbi_features.append(annotations_to_keep, ignore_index=True)

    return ncbi_features

