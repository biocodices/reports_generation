import re

import pandas as pd

from reports_generation.pipeline.vep_tsv_parsing import read_vep_tsv
from reports_generation.helpers import dictionary_deep_nan_to_None


def parse_vep_tsv(vep_tsv_filename, as_dictionary=False, collapsed=False):
    """
    Read a .tsv file of Variant Effect Predictor annotations and parse it to
    generate a pandas DataFrame. If *collapsed* is set to True, the DataFrame
    will only have two columns: 'vep_id' and 'vep_annotations' (with all the
    annotations in a single list per variant, under this last field). Otherwise,
    the dataframe will reflect the structure of the TSV.

    (The main pipeline uses collapsed=True)

    The variants are identified either by an rs ID (if present) or by their
    location (chrom:start_pos). Positions are shifted -1 in case of indels to
    match the VCF positions.

    If *as_dictionary* is set to True, the result will be returned as a dictionary
    with the variant IDs as keys and a list of annotations per variant as values:

    {
        "rs123": [
            { 'feature': 'XM_123', 'allele': 'A', 'impact': 'HIGH' ... },
            { 'feature': 'XM_234', 'allele': 'A', 'impact': 'LOW' ... },
            ...
        "1:1099": [ ... ]
    }
    """
    vep_df = read_vep_tsv(vep_tsv_filename)
    vep_df['genomic_allele'] = vep_df['allele']
    is_rs = vep_df['uploaded_variation'].str.match('^rs')
    is_snv = vep_df['variant_class'] == 'SNV'

    vep_df.loc[is_rs, 'id_or_location'] = vep_df.loc[is_rs, 'uploaded_variation']

    unknown_snvs = (~is_rs & is_snv)
    vep_df.loc[unknown_snvs, 'id_or_location'] = \
        vep_df.loc[unknown_snvs, 'location']

    unknown_indels = (~is_rs & ~is_snv)
    vep_df.loc[unknown_indels, 'id_or_location'] = \
        vep_df.loc[unknown_indels, 'location'].map(_parse_vep_indel_location)

    if as_dictionary or collapsed:
        vep_annotations_per_variant = {
            variant: variant_annotations.to_dict(orient='records')
            for variant, variant_annotations in vep_df.groupby('id_or_location')
        }
        # The pandas DataFrame usually has NaN values mixed with
        # floats. Those NaN values are moved to the dictionary
        # of annotations and will later cause malformed JSON files.
        # Thus, we convert the nan values to None here:
        vep_annotations_per_variant = \
            dictionary_deep_nan_to_None(vep_annotations_per_variant)

    if as_dictionary:
        return vep_annotations_per_variant

    if collapsed:
        vep_df = pd.Series(vep_annotations_per_variant)
        vep_df = (vep_df
            .to_frame()
            .reset_index()
            .rename(columns={'index': 'id_or_location', 0: 'vep_annotations'}))

    return vep_df


def _parse_vep_indel_location(location):
    """
    VEP annotates indels with a location like "1:1000-1020", where the
    start position 1000 is shifted +1 relative to the VCF POS[isition]
    for the same variant.

    Here, we parse that location and return the "1:999" (-1) position to match
    the VCF POS (without the end position).
    """
    range_position = re.match(r'(.+):(\d+)-?', location)
    if range_position:
        chrom, pos = range_position.groups()
        pos = int(pos) - 1
        return f'{chrom}:{pos}'
    else:
        return location
