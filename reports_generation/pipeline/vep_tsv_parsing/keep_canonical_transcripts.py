import pandas as pd


def keep_canonical_transcripts(vep_df):
    """
    Parse a dataframe of Variant Effect Predictor annotations and keep only
    the entries marked as 'canonical'. Returns a new dataframe.

    If there are no canonical entries for a given variant, raise an Exception.
    """
    canonicals = pd.DataFrame({})

    for variant_name, variant_annotations in vep_df.groupby('uploaded_variation'):
        is_canonical = (variant_annotations['canonical'] == 'YES')
        annotations_to_keep = variant_annotations[is_canonical]

        if annotations_to_keep.empty:
            raise NoCanonicalTranscript('No canonical transcripts for {}!'
                                        .format(variant_name))

        canonicals = canonicals.append(annotations_to_keep, ignore_index=True)

    return canonicals


class NoCanonicalTranscript(Exception):
    pass

