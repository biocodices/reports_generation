import pandas as pd

from reports_generation.helpers import AlleleCategory


def keep_transcripts_with_worst_impact(vep_df):
    """
    Context: VEP provides several canonical transcript annotations for the same
    variant, relating it to different nearby genes.

    Per each variant, keep only the the transcripts with the worst
    'impact'. If many transcripts share the worst category (say, many are
    predicted to have 'HIGH' impact), all of them will be kept, but not the
    rest of the same variant (say, others with 'MODERATE' impact).

    Returns a new dataframe.
    """
    worst_transcripts = pd.DataFrame({})

    for _, transcripts in vep_df.groupby('uploaded_variation'):
        categories = {impact: AlleleCategory(impact).rank
                      for impact in transcripts['impact']}
        worst_category = max(categories.keys(),
                             key=(lambda key: categories[key]))
        worst_ones = transcripts['impact'] == worst_category
        transcripts_to_keep = transcripts[worst_ones]
        worst_transcripts = worst_transcripts.append(transcripts_to_keep,
                                                     ignore_index=True,
                                                     verify_integrity=True)

    return worst_transcripts

