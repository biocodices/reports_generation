def add_VEP_genes(variant):
    """
    Receive a Series of an annotated variant with the fields
    'entrez_gene_symbols' and 'all_vep_annotations'. Take the 'symbol' under each
    of the VEP annotations and merge it to the list of Entrez gene symbols
    already present for the variant.

    Returns an updated version of that list, meant to replace
    'entrez_gene_symbols' for the given variant.
    """
    gene_symbols = variant['entrez_gene_symbols']
    gene_symbols += [entry['symbol'] for entry in variant['all_vep_annotations']]

    return sorted(set(symbol for symbol in gene_symbols if symbol))

