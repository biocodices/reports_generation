def extract_cds_changes(variant):
    """
    Given a variant with 'reportable_clinvar_variations', 'reportable_clinvar_entries',
    'reportable_snpeff_annotations' and 'reportable_vep_annotations', make a single
    list of coding sequence changes for this variant.
    """
    cds_changes = []

    for clinvar_variation in variant['reportable_clinvar_variations']:
        cds_changes += clinvar_variation.get('coding_changes', [])

    for clinvar_entry in variant['reportable_clinvar_entries']:
        cds_changes.append(clinvar_entry.get('cds_change'))

    for snpeff_annotation in variant['reportable_snpeff_annotations']:
        cds_changes.append(snpeff_annotation.get('hgvs_c'))

    for vep_annotation in variant['reportable_vep_annotations']:
        cds_change = vep_annotation.get('hgvsc')
        if not cds_change:
            continue
        if vep_annotation.get('canonical') == 'YES':
            cds_change += ' (Canonical Transcript)'
        cds_changes.append(cds_change)

    return sorted(set(c for c in cds_changes if c))

