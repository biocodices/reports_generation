from copy import deepcopy


def move_somatic_clinvar_entries_to_new_column(variant):
    """
    Given a pandas +series+ (or just a Python a dict) from a variant, move its
    'all_clinvar_entries' that have origin 'somatic' to a new column
    'somatic_clinvar_entries'.

    If no somatic entries are found, a new key 'somatic_clinvar_entries' will
    be created anyway as an empty list.

    A new variant is returned
    """
    new_variant = deepcopy(variant)

    germline_clinvar_entries = []
    somatic_clinvar_entries = []

    clinvar_entries = new_variant.get('all_clinvar_entries', [])
    for i, clinvar_entry in enumerate(clinvar_entries):
        if clinvar_entry.get('origin') == 'somatic':
            somatic_clinvar_entries.append(clinvar_entry)
        else:
            germline_clinvar_entries.append(clinvar_entry)

    new_variant['all_clinvar_entries'] = germline_clinvar_entries
    new_variant['somatic_clinvar_entries'] = somatic_clinvar_entries

    return new_variant
