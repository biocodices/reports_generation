from copy import deepcopy
from itertools import groupby
from operator import itemgetter

from more_itertools import collapse


def gather_pubmed_references(variants):
    """
    Given a dataframe of variants, gather all the PubMed references from
    OMIM and GWAS Catalog entries in a single list, and return that list.
    """
    all_pubmed_entries = []
    unique_pubmed_entries = []

    # Collect all pubmed entries (there will be duplicated items).
    # Add info about the variant where the pubmed entry came from.
    for ix, variant in variants.iterrows():
        for omim_entry in variant['omim_entries']:
            for pubmed_entry in omim_entry.get('pubmed_entries') or []:
                pubmed_entry['variants'] = [variant['order']]
                all_pubmed_entries.append(pubmed_entry)
        for gwas_entry in variant['all_gwas_associations']:
            for pubmed_entry in gwas_entry.get('pubmed_entries') or []:
                pubmed_entry['variants'] = [variant['order']]
                all_pubmed_entries.append(pubmed_entry)

    # Collapse into unique pubmed entries by PMID, keeping the variants of origin:
    sorted_pubmed_entries = sorted(all_pubmed_entries, key=itemgetter('pmid'))
    for pmid, entries in groupby(sorted_pubmed_entries, key=itemgetter('pmid')):
        entries = list(entries)
        # The entries of the same PMID should all be the same, so I just take
        # the first one here:
        collapsed_pubmed_entry = deepcopy(entries[0])
        collapsed_pubmed_entry['variants'] = \
            list(set(collapse(entry['variants'] for entry in entries)))
        unique_pubmed_entries.append(collapsed_pubmed_entry)

    # The AMA_Citation starts with the author last name, so this sort is
    # the same as sorting by author:
    return sorted(unique_pubmed_entries, key=itemgetter('AMA_Citation'))
