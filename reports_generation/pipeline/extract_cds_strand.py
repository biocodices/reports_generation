import logging

from more_itertools import one
from reports_generation.helpers import is_nan


logger = logging.getLogger(__name__)


def extract_cds_strand(variant):
    """
    Extract the coding strand ('+' or '-') from any of the 'all_vep_annotations'
    under the key 'strand' or from any of the 'dbnsfp' annotations if there is
    a key 'cds_strand'. Prefers DBNSFP data when it's available or if it does
    not match VEP's.

    It wil raise an Exception if DBNSFP data is contradictory, but it will
    ignore VEP inconsistencies silently (it's common from VEP annotations
    since a lot of transcripts are described).

    Returns None if no reliable data is available.
    """
    vep_strand = None
    dbnsfp_strand = None

    vep_to_symbol = {1: '+', -1: '-', '1': '+', '-1': '-'}
    vep_annotations = variant.get('all_vep_annotations')
    if vep_annotations:
        vep_strands = {ann['strand'] for ann in vep_annotations}
        vep_strands = {strand for strand in vep_strands
                       if strand and not is_nan(strand)}
        try:
            vep_strand = one(vep_strands)
        except ValueError:
            pass  # Ignore VEP contradictory strands
        else:
            vep_strand = vep_to_symbol[vep_strand]

    dbnsfp_info = variant.get('dbnsfp') or []
    dbnsfp_strands = {entry.get('cds_strand') for entry in dbnsfp_info}

    try:
        dbnsfp_strand = one(dbnsfp_strands)
    except ValueError:
        if len(dbnsfp_strands) > 1:
            msg = 'Variant that codes both ways according to DbNSFP? {}'
            raise StrandMismatch(msg.format(variant['dbnsfp']))

    if dbnsfp_strand and vep_strand and (dbnsfp_strand != vep_strand):
        msg = "VEP and DbNSFP strands don't match for {}: {} vs {}. Use DbNSFP."
        logger.warning(msg.format(variant['id'], vep_strand, dbnsfp_strand))
        vep_strand = None

    # Prefer DBNSFP data to VEP if it's available
    return dbnsfp_strand or vep_strand


class StrandMismatch(Exception):
    pass

