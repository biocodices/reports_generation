import logging

logger = logging.getLogger(__name__)


def prepare_gene_annotations(annotations):
    """
    Expects a pandas DataFrame of gene annotations, as produces by the
    AnnotationPipeline from anotamela.

    Adds some fields to ease the report generation downstream,
    extracting data from the 'mygene' and 'gene_entrez' dictionaries.

    Returns a new DataFrame with the extra fields.
    """
    def extract_key(dictionary, key, fallback_key=None):
        if dictionary:
            return dictionary.get(key) or dictionary.get(fallback_key)

    df = annotations.copy()

    # Make symbol, name, description and url readily accessible for the
    # template engine:
    df['symbol'] = df['gene_entrez'].apply(extract_key,
                                           key='nomenclature_symbol',
                                           fallback_key='name')

    df['name'] = df['gene_entrez'].apply(extract_key, key='nomenclature_name')

    df['description'] = df['gene_entrez'].apply(extract_key, key='summary')
    df['url'] = df['gene_entrez'].apply(extract_key, key='url')
    df['entrez_gene_id'] = df['entrez_gene_id'].astype(str)
    df['other_names'] = df['gene_entrez'].apply(extract_key, key='other_aliases')
    df['mim'] = df['mygene'].apply(extract_key, key='MIM')
    df['swissprot'] = df['mygene'].apply(extract_key, key='swissprot')
    df['hgnc'] = df['mygene'].apply(extract_key, key='HGNC')
    df['location'] = df['gene_entrez'].apply(extract_key, key='map_location')

    return df
