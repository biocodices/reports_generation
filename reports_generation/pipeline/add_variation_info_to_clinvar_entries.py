from copy import deepcopy


def add_variation_info_to_clinvar_entries(variant):
    """
    Given a *variant* with "all_clinvar_entries" (each one a dictionary),
    add information to each entry from its associated variation (via its
    "variant_id"). The associated variation will be looked among among the
    variant's "all_clinvar_variations".

    Returns a copy of the entries, modified.
    """
    modified_entries = deepcopy(variant['all_clinvar_entries'])

    for clinvar_entry in modified_entries:

        # Find the associated ClinVar Variation:
        variations = [variation for variation in variant['all_clinvar_variations']
                      if variation['variation_id'] == clinvar_entry.get('variant_id')]

        if not variations:
            continue

        variation = variations[0]

        # Add its info to the ClinVar entry
        keys_to_add = [
            'variation_name',
            'variation_type',
        ]
        for key in keys_to_add:
            clinvar_entry[key] = variation[key]

    return modified_entries
