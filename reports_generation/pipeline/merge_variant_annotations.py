import pandas as pd

from reports_generation.helpers import replace_nan_with_None


def merge_variant_annotations(annotations, vep_annotations):
    """
    Given a dataframe of annotations from the RSID-annotation pipeline and
    a dataframe of Variant Effect Predictor annotations as given by
    parse_vep_tsv(), merge them without leaving any variant out.

    Returns an updated version of the annotations dataframe with a new column
    'vep_annotations' and new entries for those IDs that were only present
    in *vep_annotations*.
    """
    merged = pd.merge(annotations, vep_annotations,
                      left_on='id', right_on='id_or_location', how='outer')

    # Use the ID-or-location from VEP-only annotations
    no_id = merged['id'].isnull()
    merged.loc[no_id, 'id'] = merged.loc[no_id, 'id_or_location']

    # The merge leaves a lot of NaN values for the variants that have a VEP
    # data but no RSID, and hence empty values for the rest of the
    # annotations. This represents a huge PITA downstream in the pipeline,
    # since NaN values evaluate to True! Thus, we transform them to None here
    # and save a lot of NaN-specific checks later:
    merged = replace_nan_with_None(merged)

    return merged
