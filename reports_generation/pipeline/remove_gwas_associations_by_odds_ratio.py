def remove_gwas_associations_by_odds_ratio(gwas_associations, min_odds_ratio):
    """
    Given a list of GWAS Catalog associations, keep the ones that have an
    'odds_ratio_per_copy' higher than *min_odds_ratio*. If no odds ratio value
    is available, keep the association as well. Returns a new list of
    associations.
    """
    associations_kept = []

    for association in gwas_associations:
        odds_ratio = association.get('odds_ratio_per_copy')
        if not odds_ratio or odds_ratio > min_odds_ratio:
            associations_kept.append(association)

    return associations_kept
