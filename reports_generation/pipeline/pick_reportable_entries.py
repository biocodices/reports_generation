from copy import deepcopy

from reports_generation.helpers import AlleleCategory


def pick_reportable_entries(variant, min_reportable_category):
    """
    Given a variant with:

        * 'sample_clinvar_variations'
        * 'sample_clinvar_entries'
        * 'sample_gwas_associations'
        * 'sample_snpeff_annotations'
        * 'sample_vep_annotations'

    Return a new version of the same variant with new fields:

        * 'reportable_clinvar_variations'
        * 'reportable_clinvar_entries'
        * 'reportable_gwas_associations'
        * 'reportable_snpeff_annotations'
        * 'reportable_vep_annotations'

    And without the original fields.

    Each new group has a subset of the original entries, keeping the ones with
    categories that are equal or greater than the *min_reportable_category*
    passed. (*min_reportable_category* must be a category tag like 'PAT'.)
    """
    min_category = AlleleCategory(min_reportable_category)

    new_variant = deepcopy(variant)

    # ----- ClinVar variations -----
    reportable_clinvar_variations = []
    for variation in variant['sample_clinvar_variations']:
        for significance in variation['clinical_significances']:
            if AlleleCategory(significance) >= min_category:
                reportable_clinvar_variations.append(variation)
                break
    new_variant['reportable_clinvar_variations'] = reportable_clinvar_variations

    # ----- ClinVar entries -----
    reportable_clinvar_entries = []
    for entry in variant['sample_clinvar_entries']:
        for significance in entry['clinical_significances']:
            if AlleleCategory(significance) >= min_category:
                reportable_clinvar_entries.append(entry)
                break
    new_variant['reportable_clinvar_entries'] = reportable_clinvar_entries

    # ----- SnpEff -----
    reportable_snpeff_annotations = [
        ann for ann in variant['sample_snpeff_annotations']
        if ann['putative_impact'] and
        AlleleCategory(ann['putative_impact']) >= min_category
    ]
    # This discards any annotation with a 'putative_impact' == None
    new_variant['reportable_snpeff_annotations'] = reportable_snpeff_annotations

    # ----- VEP -----
    reportable_vep_annotations = [
        ann for ann in variant['sample_vep_annotations']
        if ann['impact'] and AlleleCategory(ann['impact']) >= min_category
    ]
    # This discards any annotation with a impact' == None
    new_variant['reportable_vep_annotations'] = reportable_vep_annotations

    # ----- GWAS -----
    if min_category <= AlleleCategory('ASSOC'):
        reportable_gwas_associations = variant['sample_gwas_associations']
    elif (reportable_clinvar_variations or
          reportable_snpeff_annotations or
          reportable_vep_annotations):
        # Special case: if the variant is reportable because of SnpEff,
        # VEP or ClinVar, then leave the GWAS entries!
        reportable_gwas_associations = variant['sample_gwas_associations']
    else:
        reportable_gwas_associations = []

    new_variant['reportable_gwas_associations'] = reportable_gwas_associations

    return new_variant
