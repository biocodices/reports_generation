from reports_generation.helpers import is_SNP, is_nan


def has_risk_allele(variant):
    """
    Given a variant with a list of 'risk_alleles' and 'genotype_alleles',\
    determine if any of the alleles in the genotype are described as having
    risk.

    Returns True, False or None (indeterminate case).

    Indeterminate cases:

        - If a variant has no 'risk_alleles' defined.
        - If among the risk alleles or among the genotype
          alleles there are indels/duplications.

    You should treat a None result from this method as 'uncertain', different
    from False, which signals 'certitude' about a variant not having risk
    alleles.
    """
    if not variant['risk_alleles']:
        return None  # None as 'uncertain', different from False

    if is_nan(variant['risk_alleles']):
        msg = ('This genotype has risk_alleles == NaN, which should never be ' +
               'the case in this pipeline. The merge between genotypes and ' +
               'annotations probably went wrong, maybe involving a non-rsID ' +
               'variant annotation from VEP?')
        raise ValueError(msg)

    # When some the variant is not a SNP, err on the side of caution and be
    # uncertain about risk alleles being present in the genotype, since the
    # same indel or duplication might be described in many ways:
    if not is_SNP(variant):
        return None

    return any(allele in variant['risk_alleles']
               for allele in variant['genotype_alleles'])


def present_risk_alleles(variant):
    """
    Given a variant with 'genotype_alleles' and 'risk_alleles', return the list
    of risk alleles that are seen in the genotype.
    """
    return list({risk_allele
                 for risk_allele in variant['risk_alleles']
                 if risk_allele in variant['genotype_alleles']})
