import re


def define_zygosity(genotype):
    """Given a genotype like '0|0', '0/1', or '0', return the zygosity
    (e.g. "Homozygous", "Heterozygous", "Hemizygous", respectively)."""
    alleles = re.split(r'[|/]', genotype)
    if len(alleles) == 1:
        return 'Hemizygous'
    elif len(alleles) == 2:
        alleles_are_the_same = alleles[0] == alleles[1]
        return 'Homozygous' if alleles_are_the_same else 'Heterozygous'
    else:
        msg = "Couldn't parse this genotype to define zygosity: {}"
        raise Exception(msg.format(genotype))


def define_genotype_type(variant):
    """Given a variant with 'zygosity' defined and a 'genotype' value,
    determine if the genotype is reference or not. E.g. '0|0' and 'Homozygous'
    will be classified as 'Homozygous Reference'."""
    if variant['zygosity'] == 'Heterozygous':
        suffix = ''
    elif '0' in variant['genotype']:
        suffix = ' Reference'
    else:
        suffix = ' Alternative'

    return variant['zygosity'] + suffix


def define_genotype_alleles(variant):
    """Given a variant with a numeric 'genotype' like '0|0', 'vcf_ref' and
    'vcf_alt' values, generate a genotype with the allele values, like 'A|A'."""
    geno_alleles = variant['genotype']
    # REF will be 0, first ALT 1, second ALT (if present) 2, etc.
    alleles = [variant['vcf_ref']] + variant['vcf_alt']
    for j, allele in enumerate(alleles):
        geno_alleles = re.sub(r'\b%s\b' % (j), allele, geno_alleles)
    return geno_alleles


def split_genotype_string(genotype_string):
    """Given a genotype string like 'A/G', 'A|G' or 'G', return a list with the
    seen alleles: ['A', 'G'] or ['G']."""
    return re.split(r'[|/]', genotype_string)

