from collections import Counter

from reports_generation.helpers import AlleleCategory


def extract_clinvar_significances(clinvar_variations):
    """
    Take a list of ClinVar entries and return a dict of significances and
    count values for each one.

    Example:
        clinvar_variations = [
            {'clinical_significances': ['Pathogenic', ...]},
            {'clinical_significances': ['Likely pathogenic', ...]},
            {'clinical_significances': ['Likely pathogenic', ...]},
        ]

        extract_clinvar_significances(clinvar_variations)
        # => {'PAT': 1, 'LPAT': 2}
    """
    if not clinvar_variations:
        return

    significances = []
    for clinvar_variation in clinvar_variations:
        for significance in clinvar_variation['clinical_significances']:
            significances.append(significance)

    # Convert significances like 'Likely pathogenic' to tags like 'LPAT':
    tags = [AlleleCategory(sig).tag for sig in significances]
    return Counter(tags)


def clinvar_worst_significance(significances):
    """
    Take a list of significance values like [PAT, LPAT, BEN] (or a dict where
    those values are the keys) and return the worst one.

    Example:
        significances = {'PAT': 1, 'BEN': 2}
        clinvar_worst_significance(significances)  # => 'PAT'

    """
    return max(AlleleCategory(sig) for sig in significances).tag
