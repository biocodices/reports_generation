import re


def parse_cds_changes(cds_changes):
    """
    Given a list of +cds changes+, parse them so they all look like "c.123A>G"
    and return a list of unique changes.
    """
    pattern = r'(\b(c\..+))'
    parsed_changes = set()

    for change in cds_changes:
        match = re.search(pattern, change)
        parsed_change = match.group(1) if match else change
        parsed_changes.add(parsed_change)

    return list(parsed_changes)
