from copy import deepcopy

from reports_generation.helpers import reverse_complement


def parse_gwas_catalog_associations(variant):
    """
    Given a variant with 'all_gwas_associations' associations and an rs 'id',
    parse the gwas associations so that in the multisnp fields we keep only the
    annotations for the variant rs ID.

    For those fields ('genomic_alleles', 'allele_impacts', 'chrom_locations'),
    new fields with a singular version will be generated --i.e.
    'genomic_allele', 'allele_impact', 'chrom_location'.

    Returns a new list with the parsed gwas associations.
    """
    rsid = variant['id']

    gwas_associations = deepcopy(variant['all_gwas_associations'])

    # Remove any entries where the described rs IDs don't include this variant
    gwas_associations = [association for association in gwas_associations
                         if rsid in association['rsids']]

    for entry in gwas_associations:
        entry['rsid'] = rsid
        keys = ['genomic_allele', 'allele_impact', 'chrom_location', 'url']
        for key in keys:
            plural_key = key + 's'
            if plural_key in entry:
                entry[key] = entry[plural_key].get(rsid)
                # For instance:
                # entry['genomic_allele'] = entry['genomic_alleles'].get(rsid)

    # Fix the genomic allele in case it is not among the seen alleles for that
    # SNP and the alleles are reported in the reverse strand in DbSNP:
    # This fix makes sense when there are "seen alleles", i.e., when the
    # annotation is done from a VCF, where ALT and REF alleles are specified:
    if variant['is_REV'] and 'ref' in variant and 'alt' in variant:
        alt_alleles = variant['alt']
        ref_allele = variant['ref']
        snp_alleles = alt_alleles + [ref_allele]
        gwas_associations = fix_gwas_genomic_allele(gwas_associations,
                                                    snp_alleles)

    return gwas_associations


def fix_gwas_genomic_allele(gwas_associations, snp_alleles):
    """
    Takes a list of GWAS Catalog associations with a 'genomic_allele' each and
    a list of *snp_alleles*.

    It returns a list of the same associations with the 'genomic_allele'
    changed to the reverse complement if the original 'genomic_allele' wasn't
    among the *snp_alleles*.
    """
    fixed_gwas_associations = deepcopy(gwas_associations)

    for entry in fixed_gwas_associations:
        risk_allele = entry['genomic_allele']
        if risk_allele and risk_allele not in snp_alleles:
            complement_risk_allele = reverse_complement(risk_allele)
            if complement_risk_allele in snp_alleles:
                entry['genomic_allele'] = complement_risk_allele
                entry['genomic_allele_reversed'] = True

    return fixed_gwas_associations
