def count_incidental_findings(variants):
    """
    Return the number of variants that should be reported even if incidental.
    """
    counts = variants.is_incidental.value_counts()

    # The conversion to integer is necessary so this can be later serialized
    # as JSON with no problems. (Without the conversion, it would be int64,
    # because numpy/pandas.)
    return int(counts.get(True, 0))

