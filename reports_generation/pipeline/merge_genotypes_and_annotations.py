import pandas as pd


def merge_genotypes_and_annotations(genotypes, annotations):
    """
    Merge a dataframe of genotypes from a VCF and a dataframe of annotations.

    The *genotypes* must have an 'id' column with the ID present in the VCF
    (usually an RSID, but maybe some other ID or a "." representing an unknown
    variant. For the novel variants, the fields 'vcf_chrom' and 'vcf_pos'
    will be used to generate a location string like "1:1000".

    The *annotations* must have an 'id' column with either and RSID or a VEP
    based location string (like "1:1000").

    Returns a dataframe with *all* the input genotypes merged with the
    available annotations by the id or the location string.
    """
    genotypes['id_or_location'] = genotypes.apply(_id_or_location_from_VCF,
                                                  axis=1, result_type='reduce')
    merged = pd.merge(genotypes, annotations, left_on='id_or_location',
                      right_on='id', how='left')
    return merged


def _id_or_location_from_VCF(variant):
    """
    Given a VCF *variant* (as pandas Series), return its ID if it is present, or
    a string composed of CHROM:POS if the ID is missing ("."). This string is
    meant to match VEP-generated 'location' values in the annotations.
    """
    id_ = variant.get('vcf_id')

    if id_ == '.':
        return '{}:{}'.format(variant['vcf_chrom'], int(variant['vcf_pos']))
        # Warning: transform the position to integer because sometimes it's
        # read as a float by pandas.read_csv
    else:
        return id_
