from copy import deepcopy
import re


def remove_entries_by_phenotype(variant, phenos_regex_list):
    """
    Given a variant with:

        - 'reportable_clinvar_variations'
        - 'reportable_clinvar_entries'
        - 'reportable_gwas_associations'
        - 'omim_entries'

    Return a new variant were each entry in those fields is kept if it matches
    at least one relevant phenotype. Pass the phenotype regular expressions
    to use in *phenos_regex_list* (as strings). Regexes will be matched in
    with the IGNORECASE flag.

    The new variant will also have an 'exluded_phenotypes' field with the
    phenotype names that didn't match any regex.
    """
    new_variant = deepcopy(variant)
    regex_list = [re.compile(regex, flags=re.IGNORECASE)
                  for regex in phenos_regex_list]

    excluded_phenotype_names = []

    kept_clinvar_variations, nonmatching_phenos_clinvar_variations = \
        _filter_clinvar_variations_by_phenotype(
            variant['reportable_clinvar_variations'],
            regex_list
        )

    kept_clinvar_entries, nonmatching_phenos_clinvar = \
        _filter_clinvar_by_phenotype(variant['reportable_clinvar_entries'],
                                     regex_list)

    kept_gwas_associations, nonmatching_phenos_gwas = \
        _filter_gwas_by_phenotype(variant['reportable_gwas_associations'],
                                  regex_list)

    kept_omim_entries, nonmatching_phenos_omim = \
        _filter_omim_by_phenotype(variant['omim_entries'], regex_list)

    new_variant['reportable_clinvar_variations'] = kept_clinvar_variations
    new_variant['reportable_clinvar_entries'] = kept_clinvar_entries
    new_variant['reportable_gwas_associations'] = kept_gwas_associations
    new_variant['omim_entries'] = kept_omim_entries

    excluded_phenotype_names = (nonmatching_phenos_omim +
                                nonmatching_phenos_gwas +
                                nonmatching_phenos_clinvar +
                                nonmatching_phenos_clinvar_variations)

    new_variant['excluded_phenotypes'] = sorted(set(excluded_phenotype_names))

    return new_variant


def _filter_omim_by_phenotype(omim_entries, regex_list):
    """
    Given *omim_entries* and a list of compiled regex, keep the entries
    where at least one phenotype name matches at least of the regex.
    """
    kept_omim_entries = []
    nonmatching_phenotype_names = []

    for omim_entry in omim_entries:
        entry = deepcopy(omim_entry)
        kept_phenotypes = []

        for phenotype in omim_entry.get('phenotypes', []):
            name = phenotype['name']
            if any(regex.search(name) for regex in regex_list):
                kept_phenotypes.append(phenotype)
            else:
                nonmatching_phenotype_names.append(name)

        if kept_phenotypes:
            # Replace all phenotypes with only the filter-passing ones
            entry['phenotypes'] = kept_phenotypes
            kept_omim_entries.append(entry)

    return (kept_omim_entries, nonmatching_phenotype_names)


def _filter_gwas_by_phenotype(gwas_associations, regex_list):
    """
    Given *gwas_associations* and a list of compiled regex, keep the entries
    where the trait matches at least one of the regex.
    """
    kept_gwas_associations = []
    nonmatching_phenotype_names = []

    for gwas_association in gwas_associations:
        name = gwas_association.get('trait', '')
        if any(regex.search(name) for regex in regex_list):
            kept_gwas_associations.append(gwas_association)
        else:
            nonmatching_phenotype_names.append(name)

    return (kept_gwas_associations, nonmatching_phenotype_names)


def _filter_clinvar_by_phenotype(clinvar_entries, regex_list):
    """
    Given *clinvar_entries* and a list of compiled regex, keep the entries
    with at least one trait that matches at least one of the regex.
    """
    kept_clinvar_entries = []
    nonmatching_phenotype_names = []

    for clinvar_entry in clinvar_entries:
        entry = deepcopy(clinvar_entry)
        kept_conditions = []

        for condition in clinvar_entry.get('conditions', []):
            name = condition['name']
            if any(regex.search(name) for regex in regex_list):
                kept_conditions.append(condition)
            else:
                nonmatching_phenotype_names.append(name)

        if kept_conditions:
            # Replace all conditions with only the filter-passing ones
            entry['conditions'] = kept_conditions
            kept_clinvar_entries.append(entry)

    return (kept_clinvar_entries, nonmatching_phenotype_names)


def _filter_clinvar_variations_by_phenotype(clinvar_variations, regex_list):
    """
    Given *clinvar_variations* and a list of compiled regex, keep the entries
    with at least one phenotype that matches at least one of the regex.
    """
    kept_clinvar_variations = []
    nonmatching_phenotype_names = []

    for clinvar_variation in clinvar_variations:
        variation = deepcopy(clinvar_variation)
        kept_phenotype_names = []

        for phenotype_name in clinvar_variation['associated_phenotypes']:
            if any(regex.search(phenotype_name) for regex in regex_list):
                kept_phenotype_names.append(phenotype_name)
            else:
                nonmatching_phenotype_names.append(phenotype_name)

        if kept_phenotype_names:
            variation['associated_phenotypes'] = kept_phenotype_names
            kept_clinvar_variations.append(variation)

    return (kept_clinvar_variations, nonmatching_phenotype_names)

