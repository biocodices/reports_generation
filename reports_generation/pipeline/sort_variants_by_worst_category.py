from reports_generation.helpers import AlleleCategory


def sort_variants_by_worst_category(variants):
    """Given a dataframe of variants with 'worst_category', sort them in
    descending order of category rank (i.e. by pathogenicity). Also add a
    new field 'order' with the order starting from 1."""
    variants['worst_category_rank'] = \
        variants['worst_category'].map(AlleleCategory.get_category_rank)
    variants = variants.sort_values(by='worst_category_rank', ascending=False)
    variants.drop('worst_category_rank', axis=1, inplace=True)

    variants.reset_index(inplace=True, drop=True)
    variants['order'] = variants.index + 1

    return variants

