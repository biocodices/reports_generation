from itertools import chain


def affected_genes_annotations(variants, gene_annotations):
    """
    Given a dataframe of *variants* and another of *gene_annotations*,
    list all the genes found in 'entrez_gene_ids' and 'all_vep_annotations' of the
    variants and return the gene annotations for those genes (filtered by
    'entrez_gene_id').

    Returns a new dataframe, subset of gene annotations, filtered in that way.
    """
    # 'entrez_gene_ids' is a Series with lists at each value, whereas
    # 'all_vep_annotations' is more complex: 'gene' in it might be an Entrez id,
    # which we want to keep, or an Ensemble id, which we don't want.
    entrez_ids = list(chain(*variants['entrez_gene_ids'].dropna()))

    for vep_annotations in variants['all_vep_annotations'].dropna():
        for annotation in vep_annotations:
            gene = annotation['gene']
            if not str(gene).startswith('ENS'):
                entrez_ids.append(gene)

    entrez_ids = {id_ for id_ in entrez_ids if id_}

    is_affected = gene_annotations['entrez_gene_id'].isin(entrez_ids)
    return gene_annotations[is_affected].reset_index(drop=True)
