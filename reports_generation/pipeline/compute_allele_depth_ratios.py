def compute_allele_depth_ratios(variant):
    """
    Given a variant with 'allele_depth' (a list of integers) and 'read_depth'
    (an integer), compute the percentages using 'read_depth' as totals.

    Example:
        variant = {'read_depth': 100, 'allele_depth': [50, 40]}
        compute_allele_depth_ratios(variant)
        # => [.5, .4]
    """
    return [allele_depth/variant['read_depth']
            for allele_depth in variant['allele_depth']]

