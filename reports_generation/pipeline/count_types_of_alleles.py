from collections import OrderedDict

from reports_generation.helpers import AlleleCategory


def count_types_of_alleles(categories):
    """
    Given a list of allele categories, return counts of alleles per category
    tag and description, in tuples like:

    # > count_types_of_alleles(['PAT', 'LPAT', 'HIGH'])

    # => [('PAT', 'Pathogenic', 2),
          ('LPAT', 'Likely pathogenic', 1),
          ('HIGH', 'High impact', 4)]

    The resulting dict keeps the order of the *categories*.
    """
    counts = OrderedDict()

    for tag in categories:
        category = AlleleCategory(tag)
        counts[category] = 1 if category not in counts else counts[category] + 1

    # Return one 3-items tuple for each category count:
    return [(cat.tag, cat.description, count) for cat, count in counts.items()]

