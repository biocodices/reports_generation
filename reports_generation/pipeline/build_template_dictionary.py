from itertools import chain

from reports_generation.pipeline import (
    gather_pubmed_references,
    count_types_of_alleles,
    count_incidental_findings,
)
from reports_generation.helpers import dictionary_deep_nan_to_None


def build_template_dictionary(sample_id, variants, genes, thresholds):
    """
    Given dataframes of annotated *variants* and *genes*, build a dictionary of
    information to be used in the HTML report template. The purpose here is to
    make the information readily accessible for the template engine, so that
    no logic is put in the template.
    """
    template_data = {
        'sample': sample_data(sample_id),
        'category_counts': count_types_of_alleles(variants['worst_category']),
        'incidental_findings_count': count_incidental_findings(variants),
        'genes': genes.to_dict('records'),
        'references': gather_pubmed_references(variants),
        'thresholds': thresholds,
        'excluded_phenotypes': gather_excluded_phenotypes(variants),
    }

    template_data['variants'] = variants.to_dict('records')

    # This step is necessary to avoid invalid JSON problems downstream:
    template_data['variants'] = [dictionary_deep_nan_to_None(variant)
                                 for variant in template_data['variants']]

    return template_data


def gather_excluded_phenotypes(variants):
    """
    Given a DataFrame of variants, gather all their 'excluded_phenotypes' in a
    single unique list.
    """
    try:
        return sorted(set(chain.from_iterable(variants['excluded_phenotypes'])))
    except KeyError:
        return []


def sample_data(sample_id):
    # TODO: fetch sample info from database
    return {
        'id': sample_id,
        'full_name': None,
        'age': None,
        'sex': None,
        'institution_name': None,
        'doctor_full_name': None,
        'date_of_admission': None,
        'date_of_report': None,
    }
