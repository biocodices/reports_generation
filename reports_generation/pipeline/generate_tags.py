from more_itertools import unique_everseen


def generate_tags(variant):
    """
    Given a variant, generate a list of tags informative about various
    conditions like 'has_clinvar_entries', 'uncertain_risk_allele', etc.
    """
    tags = []

    entries_keys = [
        'reportable_clinvar_variations',
        'omim_entries',
        'reportable_gwas_associations',
        'reportable_snpeff_annotations',
        'reportable_vep_annotations',
    ]

    for key in entries_keys:
        if variant[key]:
            tags.append(key.replace('_', ' ').replace('reportable ', ''))

        if 'reportable_' in key:
            for entry in variant[key]:
                if not entry['genomic_allele']:
                    tags.append('entries with uncertain allele')

    some_filters_didnt_pass = False
    if 'FT' in variant and variant['FT'] != 'PASS':
        some_filters_didnt_pass = True
    if variant['vcf_filter'] != 'PASS':
        some_filters_didnt_pass = True
    if some_filters_didnt_pass:
        tags.append("filters not passed")


    if variant['is_REV']:
        tags.append('is REV')

    if not variant['risk_alleles']:
        tags.append('uncertain risk allele')

    if not variant['present_risk_alleles']:
        tags.append('sample might not have the risk allele')

    snp_alleles = [variant['vcf_ref']] + variant['vcf_alt']
    for risk_allele in variant['present_risk_alleles']:
        if risk_allele not in snp_alleles:
            tags.append('risk allele not seen in VCF')

    if variant['gmaf'] and variant['gmaf'] > 0.05:
        tags.append('high frequency alleles')

    if any(risk_allele == variant['vcf_ref']
           for risk_allele in variant['present_risk_alleles']):
        tags.append('REF risk allele')

    if not variant['all_clinvar_variations']:
        tags.append('not in clinvar')

    return list(unique_everseen(tags))
