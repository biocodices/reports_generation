from operator import attrgetter
import logging

import coloredlogs

from reports_generation.pipeline import (
    define_zygosity,
    define_genotype_type,
    define_genotype_alleles,
    split_genotype_string,
    has_risk_allele,
    extract_cds_changes,
    parse_cds_changes,
    extract_prot_changes,
    parse_prot_changes,
    filter_by_present_risk_alleles,
    remove_gwas_associations_by_odds_ratio,
    pick_reportable_entries,
    remove_high_frequency_alleles,
    remove_entries_by_phenotype,
    group_snpeff_annotations,
    group_vep_annotations,
    group_clinvar_entries,
    present_risk_alleles,
    extract_clinvar_significances,
    clinvar_worst_significance,
    extract_worst_category,
    pick_worst_category,
    extract_clinvar_phenotype_names,
    extract_clinvar_variation_phenotype_names,
    extract_gwas_phenotype_names,
    extract_omim_phenotype_names,
    extract_phenotype_names,
    generate_hgvs_g,
    add_ref_hgvs_g,
    compute_allele_depth_ratios,
    parse_frequencies_from_INFO,
    merge_frequencies,
)

from reports_generation.helpers import (
    extract_key_of_all_dicts,
)


logger = logging.getLogger(__name__)
coloredlogs.install(level='INFO')


def prepare_sample_genotypes(annotated_genotypes,
                             sample_id,
                             min_reportable_category='BEN',
                             min_odds_ratio=1,
                             max_frequency=1,
                             phenos_regex_list=None):
    """
    From a DataFrame of *annotated_genotypes* of a given *sample_id*, add extra
    annotations based on the found genotype/annotations in each row and filter
    based both on sample genotype and passed thresholds.

    Returns a new dataframe with the reportable sample variants 'prepared'
    for the report.
    """
    def log(msg):
        logger.info('Sample {}: {}'.format(sample_id, msg))

    df = annotated_genotypes.copy()

    log('Parse VCF frequencies from the INFO field')
    df['vcf_frequencies'] = df.apply(parse_frequencies_from_INFO, axis=1)

    log('Merge VCF frequencies with annotation frequencies')
    df['frequencies'] = df.apply(merge_frequencies, axis=1)

    log('Define zygosity')
    df['zygosity'] = df['genotype'].map(define_zygosity)

    log('Define genotype type')
    df['genotype_type'] = df.apply(define_genotype_type, axis=1)

    log('Translate genotype numbers to alleles')
    df['genotype_alleles_string'] = df.apply(define_genotype_alleles, axis=1)

    log('Split the genotype string')
    df['genotype_alleles'] = (df['genotype_alleles_string']
                              .map(split_genotype_string))

    df['allele_depth_ratios'] = \
        df.apply(compute_allele_depth_ratios, axis=1)

    log('Check if each genotype has any risk alleles')
    df['has_risk_allele'] = df.apply(has_risk_allele, axis=1)

    log('List the present risk alleles')
    df['present_risk_alleles'] = df.apply(present_risk_alleles, axis=1,
                                          result_type='reduce')

    log('Add a pseudo-HGVS genomic change for reference alleles')
    df['hgvs'] = df.apply(add_ref_hgvs_g, axis=1)

    log('Keep HGVS annotations of risk alleles in the sample')
    df['sample_hgvs'] = df.apply(filter_by_present_risk_alleles, axis=1,
                                 result_type='reduce', annotations_key='hgvs')

    log('Generate an HGVS genomic change for the alleles lacking one')
    no_hgvs = df['sample_hgvs'].isnull()
    if any(no_hgvs):
        hgvs_values = df[no_hgvs].apply(generate_hgvs_g, axis=1,
                                        result_type='reduce')
        df.loc[no_hgvs, 'sample_hgvs'] = hgvs_values

    # Filtering of entries according to present risk alleles #

    log('Keep onty the SnpEff annotations for the genotype risk alleles')
    df['sample_snpeff_annotations'] = \
        df.apply(filter_by_present_risk_alleles, axis=1, result_type='reduce',
                 annotations_key='all_snpeff_annotations')

    log('Keep onty the VEP annotations for the genotype risk alleles')
    df['sample_vep_annotations'] = \
        df.apply(filter_by_present_risk_alleles, axis=1, result_type='reduce',
                 annotations_key='all_vep_annotations')

    log('Keep only the ClinVar entries for the genotype risk alleles')
    df['sample_clinvar_entries'] = \
        df.apply(filter_by_present_risk_alleles, axis=1, result_type='reduce',
                 annotations_key='all_clinvar_entries')

    log('Keep only the ClinVar variations for the genotype risk alleles')
    df['sample_clinvar_variations'] = \
        df.apply(filter_by_present_risk_alleles, axis=1, result_type='reduce',
                 annotations_key='all_clinvar_variations')

    log('Keep only the GWAS Catalog associations for the genotype risk alleles')
    df['sample_gwas_associations'] = \
        df.apply(filter_by_present_risk_alleles, axis=1, result_type='reduce',
                 annotations_key='all_gwas_associations')

    # Filtering of entries according to pipeline thresholds #

    log('Remove Snpeff, VEP, GWAS, ClinVar entries, ClinVar variations '
        'with category < {}'.format(min_reportable_category))
    df = df.apply(pick_reportable_entries, axis=1, result_type='reduce',
                  min_reportable_category=min_reportable_category)

    log('Remove Snpeff, VEP, GWAS and ClinVar entries, Clinvar variations '
        'about alleles with a frequency <= {}'.format(max_frequency))
    df = df.apply(remove_high_frequency_alleles, axis=1, result_type='reduce',
                  max_frequency=max_frequency)

    log('Remove GWAS associations with odds ratio < {}'.format(min_odds_ratio))
    df['reportable_gwas_associations'] = (df['reportable_gwas_associations'].
                                          apply(remove_gwas_associations_by_odds_ratio,
                                                min_odds_ratio=min_odds_ratio))

    if phenos_regex_list:
        log('Remove GWAS, OMIM, Clinvar entries, Clinvar Variations by '
            'phenotype name with {} regexes: {}'
            .format(len(phenos_regex_list), ' | '.join(phenos_regex_list)))
        df = df.apply(remove_entries_by_phenotype, axis=1, result_type='reduce',
                      phenos_regex_list=phenos_regex_list)

    # Extraction of data to make the dictionary "flatter" #

    log('Extract ClinVar significances')
    df['clinvar_significances'] = (df['reportable_clinvar_variations']
                                   .map(extract_clinvar_significances))

    log('Extract ClinVar worst significance')
    df['clinvar_worst_significance'] = (df['clinvar_significances']
                                        .map(clinvar_worst_significance,
                                             na_action='ignore'))

    log('Extract Snpeff impacts')
    df['snpeff_impacts'] = (df['reportable_snpeff_annotations']
                            .apply(extract_key_of_all_dicts,
                                   key='putative_impact'))

    log('Extract Snpeff worst impact')
    df['snpeff_worst_impact'] = (df['snpeff_impacts']
                                 .map(extract_worst_category,
                                      na_action='ignore'))

    log('Extract VEP impacts')
    df['vep_impacts'] = (df['reportable_vep_annotations']
                            .apply(extract_key_of_all_dicts, key='impact'))

    log('Extract VEP worst impact')
    df['vep_worst_impact'] = df['vep_impacts'].map(extract_worst_category,
                                                   na_action='ignore')

    log("Pick each variant's worst category tag and description")
    worst_categories = df.apply(pick_worst_category, axis=1, result_type='reduce')
    df['worst_category'] = worst_categories.map(attrgetter('tag'))
    df['worst_category_description'] = (worst_categories
                                        .map(attrgetter('description')))

    log('Extract phenotype names')
    df['gwas_phenotype_names'] = (df['reportable_gwas_associations']
                                  .map(extract_gwas_phenotype_names))
    df['clinvar_phenotype_names'] = (df['reportable_clinvar_entries']
                                     .map(extract_clinvar_phenotype_names))
    df['clinvar_variation_phenotype_names'] = (
        df['reportable_clinvar_variations']
        .map(extract_clinvar_variation_phenotype_names)
    )
    df['omim_phenotype_names'] = (df['omim_entries']
                                  .map(extract_omim_phenotype_names))
    df['phenotype_names'] = df.apply(extract_phenotype_names, axis=1,
                                     result_type='reduce')

    log('Extract and filter coding sequence changes')
    df['cds_changes'] = df.apply(extract_cds_changes, axis=1, result_type='reduce')

    log('Parse and unify cds changes')
    df['parsed_cds_changes'] = df['cds_changes'].map(parse_cds_changes)

    log('Extract protein changes')
    df['prot_changes'] = df.apply(extract_prot_changes, axis=1, result_type='reduce')

    log('Parse and unify prot changes')
    df['parsed_prot_changes'] = df['prot_changes'].map(parse_prot_changes)

    # Grouping of entries to facilitate report generation #

    log('Group Snpeff annotations')
    df['grouped_snpeff_annotations'] = (df['reportable_snpeff_annotations']
                                        .map(group_snpeff_annotations))

    log('Group VEP annotations')
    df['grouped_vep_annotations'] = (df['reportable_vep_annotations']
                                     .map(group_vep_annotations))

    log('Group ClinVar entries')
    df['grouped_clinvar_entries'] = (df['reportable_clinvar_entries']
                                     .map(group_clinvar_entries))

    return df
