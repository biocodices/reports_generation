def make_dbsnp_url(rsid):
    url = 'https://www.ncbi.nlm.nih.gov/projects/SNP/snp_ref.cgi?rs={}'
    return url.format(rsid)

