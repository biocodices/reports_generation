from copy import deepcopy


def unify_omim_phenotypes(omim_entries):
    """
    Given a list of OMIM entries, generate a new list where each entry's
    'phenotypes' has a list of all the seen phenotypes. This is needed because
    the original entries have both 'phenotypes' and 'phenotype_names' which
    need to be unified in case they share duplicates.
    """
    new_entries = deepcopy(omim_entries)

    for entry in new_entries:
        seen_phenotypes = [pheno['name'].lower()
                           for pheno in entry['phenotypes']]

        for pheno_name in (entry.get('phenotype_names') or []):
            if pheno_name.lower() not in seen_phenotypes:
                entry['phenotypes'].append({'name': pheno_name})

    return new_entries

