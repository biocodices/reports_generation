from copy import deepcopy


def remove_high_frequency_alleles(variant, max_frequency):
    """
    Given a variant with 'frequencies' like:
        {
            'A': {
                'dbSNP': {'General': 0.1},
                'CADD_1000g': {'African': 0.1}},
                'dbNSFP_ExAC': {'General': 0.9}
            }
        }

    And with entries that refer to a 'genomic_allele':

        - 'reportable_gwas_associations': [{'genomic_allele': 'A', ...}, { ... }]
        - 'reportable_snpeff_annotations'
        - 'reportable_vep_annotations'
        - 'reportable_snpeff_annotations'

    Return a copy of the variant where entries in each of those fields are kept
    only if their 'genomic_allele' has at least one population frequency less
    than or equal to the *max_freq* passed.

    If no frequency data can be found, the variant will be kept.
    """
    frequencies = variant['frequencies']

    if not frequencies:
        return variant

    filtered_variant = deepcopy(variant)

    fields = [
        'reportable_clinvar_entries',
        'reportable_clinvar_variations',
        'reportable_snpeff_annotations',
        'reportable_gwas_associations',
        'reportable_vep_annotations',
    ]
    fields = [field for field in fields if field in variant]

    for field in fields:
        entries = variant[field]
        filtered_entries = []

        for entry in entries:
            allele = entry.get('genomic_allele')
            allele_frequencies = frequencies.get(allele)

            # Don't filter out entries when there's uncertainty about
            # the described allele or its frequency:
            if not allele or not allele_frequencies:
                filtered_entries.append(entry)
                continue

            # Check *every* population frequency and keep the entry if the
            # frequency is acceptable in *any* of the populations:
            keep_the_entry = False
            for freq_per_population in allele_frequencies.values():
                for frequency in freq_per_population.values():
                    if frequency <= max_frequency:
                        keep_the_entry = True

            if keep_the_entry:
                filtered_entries.append(entry)

        filtered_variant[field] = filtered_entries

    return filtered_variant
