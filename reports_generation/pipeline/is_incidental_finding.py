def is_incidental_finding(omim_entries):
    """
    Given a list of OMIM entries, return True if any entry has combination
    of both a gene and a phenotype marked as 'incidental'.
    """
    for omim_entry in omim_entries:
        if not omim_entry['incidental_gene']:
            continue

        for phenotype in omim_entry.get('phenotypes') or []:
            if phenotype.get('incidental'):
                return True

    return False
