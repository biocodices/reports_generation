def snp_alleles_reported_reverse(dbsnp_web):
    """
    Return True if the alleles of the SNP are reported in the reverse
    strand in DbSNP Web.

    Returns False if no data is passed.
    """
    if not dbsnp_web:
        return False

    return bool(dbsnp_web.get('GRCh37.p13_reverse') or
                dbsnp_web.get('GRCh38.p7_reverse'))

