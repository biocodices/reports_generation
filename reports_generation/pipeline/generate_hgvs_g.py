def generate_hgvs_g(variant):
    """
    Given a variant with 'vcf_chrom', 'vcf_pos', 'vcf_ref' and 'vcf_alt' keys,
    generate a list of HGVS genomic changes like 'chr1:123A>T' (one for each
    'alt' allele). Returns a list of dictionaries:

        > variant = {'vcf_chrom': '1', 'vcf_pos': 123, 'vcf_ref': 'A',
                     'vcf_alt': ['T', 'G']}
        > generate_hgvs_g(variant)
        # => [{'genomic_allele': 'T', 'hgvs_g': 'chr1:123A>T'},
              {'genomic_allele': 'G', 'hgvs_g': 'chr1:123A>G'}]
    """
    hgvs_entries = []

    for alt_allele in variant['vcf_alt']:
        genomic_change = ('chr{vcf_chrom}:{vcf_pos}{vcf_ref}>{alt_allele}'
                          .format(**variant, alt_allele=alt_allele))
        entry = {'genomic_allele': alt_allele, 'hgvs_g': genomic_change}
        hgvs_entries.append(entry)

    return hgvs_entries


def add_ref_hgvs_g(variant):
    """
    Given a variant with 'vcf_chrom', 'vcf_pos', 'vcf_ref' keys and 'hgvs'
    entries, add a new hgvs pseudo-entry for the reference allele. Returns a
    new 'hgvs' field with the old contents of that field plus the new ref one.

        > variant = {'vcf_chrom': '1', 'vcf_pos': 123, 'vcf_ref': 'A',
                     'vcf_alt': ['T', 'G'], 'hgvs': [ ... ]}
        > add_ref_hgvs_g(variant)
        # => { ... ,
              'hgvs': [ ..., {'genomic_allele': 'A', 'hgvs_g': 'chr1:123A='}]}
    """
    hgvs_entries = list(variant['hgvs'])
    has_ref_entry = any(entry['genomic_allele'] == variant['vcf_ref']
                        for entry in hgvs_entries)
    if not has_ref_entry:
        ref_entry = {'genomic_allele': variant['vcf_ref'],
                     'hgvs_g': 'chr{vcf_chrom}:{vcf_pos}{vcf_ref}='.format(**variant)}
        hgvs_entries.append(ref_entry)

    return hgvs_entries
