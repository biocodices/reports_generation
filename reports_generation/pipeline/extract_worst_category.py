from reports_generation.helpers import AlleleCategory


def extract_worst_category(categories):
    """
    Take an iterable of pathogenicity categories like [LOW, HIGH, BEN]
    and return the single worst category's tag.

    Example:

        categories = {'LOW': 2, 'MODERATE': 1}
        extract_worst_category(categories)  # => 'MODERATE'

        categories = ['PAT', 'LPAT']
        extract_worst_category(categories)  # => 'PAT'
    """
    return max(AlleleCategory(category) for category in categories).tag

