def remove_haplotype_associations(gwas_associations):
    """
    Given a list of GWAS associations, remove the ones marked as
    'multisnp_haplotype'. Returns a new list with the kept associations.
    """
    return [association for association in gwas_associations
            if not association.get('multisnp_haplotype')]

