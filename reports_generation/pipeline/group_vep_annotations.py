from collections import OrderedDict

from reports_generation.helpers import group_annotations_by_fields, AlleleCategory


def group_vep_annotations(vep_annotations):
    """
    Given a list of VEP annotations, group them by

        - impact (in descending order)
        - symbol (i.e. gene)

    Returns an OrderedDict where the keys are compounded by impact:symbol.

    {
        'LOW:GENE1': [ some annotations ],
        'HIGH:GENE1': [ some other annotations ],
    }
    """
    if not vep_annotations:
        return {}

    fields = 'impact symbol'.split()
    by_fields = group_annotations_by_fields(vep_annotations, fields)
    # The 'by_fields' grouped annotations are a dict like:
    # {'LOW:GENE-1, [ ... some annotations ... ], 'HIGH:GENE-1': [ ... ]}
    # To sort by impact, we extract the impact value from the first part of the keys:
    sorted_ = sorted(by_fields.items(), key=_sort_function)

    return OrderedDict(sorted_)


def _sort_function(key_value):
    """
    Return the AlleleCategory of a key_value pair where the key's first part
    is the impact category (separated by ':'), like 'HIGH:GENE1'.
    """
    key = key_value[0]
    impact_category, _ = key.split(':')
    return AlleleCategory(impact_category)

