from operator import itemgetter
from itertools import groupby
from collections import defaultdict, OrderedDict

from reports_generation.helpers import group_annotations_by_fields


def group_snpeff_annotations(snpeff_annotations):
    """
    Given a list of snpeff annotations, group them by biotype (so that
    intergenic mutations are grouped), cds change & prot change & gene,
    (so that similar coding mutations are grouped), and by their predicted
    impact. Returns a defaultdict of OrderedDicts!
    """
    if not snpeff_annotations:
        return {}

    fields = 'transcript_biotype gene_id hgvs_c hgvs_p effects'.split()
    by_fields = group_annotations_by_fields(snpeff_annotations, fields)

    grouped_snpeff_annotations = defaultdict(OrderedDict)

    for variant, variant_entries in by_fields.items():
        # After grouping by the previous fields, now make subgroups by the
        # predicted impact of the variants (HIGH, MODERATE, etc.)
        by_impact = groupby(variant_entries, key=itemgetter('putative_impact'))
        grouped_snpeff_annotations[variant] = {impact: list(impact_annotations)
                                               for impact, impact_annotations
                                               in by_impact}

    return grouped_snpeff_annotations

