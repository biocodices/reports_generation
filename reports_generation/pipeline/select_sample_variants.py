def select_sample_variants(sample_variants):
    """
    Given a dataframe of *sample_variants*, keep the entries that:

        - Have reportable ClinVar variations
          (checks the field 'reportable_clinvar_variations')

          OR

        - Have reportable GWAS Catalog associations
          (checks the field 'reportable_gwas_associations')

          OR

        - Don't have ClinVar data *at all* (not only 'reportable' but in
          general, checks the field 'all_clinvar_variations')

            AND

            - Have OMIM entries ('omim_entries')

              OR

            - Have reportable SnpEff annotations ('reportable_snpeff_annotations')

              OR

            - Have reportable VEP annotations ('reportable_vep_annotations')

    'Reportable' here means that the entries are about alleles:

        (1) present in the sample's genotype,

        AND

        (2) (a) with clinical significance, SnpeEff impact, or VEP impact
                above the minimum reportable category,
                OR
            (b) with a gwas association with odds ration > minimum reportable
                odds ratio

        AND

        (3) with allele frequency below the maximum frequency threshold.

    These filters/thresholds were already applied by this point, so this
    method only checks for presence or absence of entries.

    This is the rationale behind the implementation:

    When a variant has ClinVar data but no 'reportable' ClinVar variations,
    that means it is classified in ClinVar as 'benign' or smth like that (more
    precisely, a category below the chosen threshold). In those cases, we can
    ignore OMIM, SnpEff and VEP safely --in ClinVar we trust!

    Only when no ClinVar data is available *at all* (besides if it is
    'reportable') we resort to OMIM, SnpEff and VEP to check wether something
    has been said (OMIM) or predicted (SnpEff, VEP) about the variant, and
    then we include it if it's relevant.
    """
    df = sample_variants
    _safety_checks(df)

    has_reportable_clinvar_variations = df['reportable_clinvar_variations'].map(bool)
    has_reportable_gwas_entries = df['reportable_gwas_associations'].map(bool)
    has_no_clinvar_data_at_all = ~df['all_clinvar_variations'].map(bool)
    has_omim_entries = df['omim_entries'].map(bool)
    has_reportable_snpeff_entries = df['reportable_snpeff_annotations'].map(bool)
    has_reportable_vep_entries = df['reportable_vep_annotations'].map(bool)

    selected = df[
        has_reportable_clinvar_variations |
        has_reportable_gwas_entries |
        (
            has_no_clinvar_data_at_all & (
                has_omim_entries |
                has_reportable_snpeff_entries |
                has_reportable_vep_entries
            )
        )
    ]

    return selected.reset_index(drop=True)


def _safety_checks(df):
    # This code assumes all variants described belong to the same sample ID
    sample_id = df.iloc[0].sample_id
    assert all(df.sample_id == sample_id)

    # No clinvar_variations, omim_entries or gwas entries should be NaN/None.
    # This code assumes that when there are no ClinVar entries, it will find
    # an empty list:
    assert all(df['reportable_clinvar_variations'].notnull())
    assert all(df['omim_entries'].notnull())
    assert all(df['reportable_gwas_associations'].notnull())
