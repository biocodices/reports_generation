from collections import defaultdict


def parse_frequencies_from_INFO(variant):
    """
    Given a variant with:
     - 'vcf_info': a dictionary originally from the INFO of the annotated VCF
     - 'vcf_ref' allele (string)
     - 'vcf_alt' alelles (list)

    Extract and parse the population frequencies per allele. Note that
    AF is always ALTERNATE allele frequency!

    Returns a dictionary like:

    {
        'A': {
            'AF': 0.01,
            'AF_popmax': 0.03,
            'AF_raw': 0.01,
            ...
        },
        'G': {
            'AF': 0.99,
            'AF_popmax': 0.97,
            'AF_raw': 0.99,
        }
    }

    The REF allele frequencies are computed simply as (1 - p).

    Deals with multiallelic variants as well!

    If the variant is multiallelic, we still assume only one annotation of
    frequencies that applies to every ALT allele (this remains to be
    confirmed).
    """
    ref_allele, alt_alleles = (variant['vcf_ref'], variant['vcf_alt'])
    info = variant.get('vcf_info', {})
    info_frequencies = {k: v for k, v in info.items() if 'AF' in k}

    frequencies_per_allele = defaultdict(dict)

    for frequency_tag, alt_frequencies in info_frequencies.items():
        # Obs: Frequency_tag usually involve population names like "AF_afr".

        # NOTE: frequencies for multiallelic variants come merged with a ","
        # e.g. "AF=0.25,0.35" for two ALT alleles. We need to split those:
        alt_frequencies = alt_frequencies.split(',')

        # There can be missing frequency values: for those, we assume it's
        # such a low frequency that we can say it's 0. We do this to
        # prevent breaking the computation of the REF frequency below.
        alt_frequencies = [0 if freq == '.' else float(freq)
                           for freq in alt_frequencies]

        for alt_allele, frequency in zip(alt_alleles, alt_frequencies):
            check_valid_frequency(frequency)
            frequencies_per_allele[alt_allele][frequency_tag] = frequency

        ref_frequency = 1 - sum(alt_frequencies)
        check_valid_frequency(ref_frequency)
        frequencies_per_allele[ref_allele][frequency_tag] = ref_frequency

    # Prefer dict over defauldict for later JSONification without problems:
    return dict(frequencies_per_allele)

def check_valid_frequency(frequency):
    if frequency < 0 or frequency > 1:
        raise ValueError(f"Frequency outside (0, 1): {frequency}")
