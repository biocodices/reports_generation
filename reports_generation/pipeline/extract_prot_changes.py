def extract_prot_changes(variant):
    """
    Given a variant with keys 'sample_clinvar_variations',
    'sample_clinvar_entries', 'sample_snpeff_annotations',
    'uniprot_entries', 'omim_entries', and 'sample_vep_annotations',
    extract a single list of the protein changes mentioned throughout
    the annotations.
    """
    changes = []

    for variation in variant['sample_clinvar_variations']:
        changes += variation.get('protein_changes', [])

    for entry in variant['sample_clinvar_entries']:
        changes.append(entry.get('prot_change'))

    for annotation in variant['sample_snpeff_annotations']:
        changes.append(annotation.get('hgvs_p'))

    for annotation in variant['sample_vep_annotations']:
        prot_change = annotation.get('hgvsp')
        if not prot_change:
            continue
        if annotation.get('canonical') == 'YES':
            prot_change += ' (Canonical Transcript)'
        changes.append(prot_change)

    for entry in variant['uniprot_entries']:
        changes.append(entry.get('prot_change'))

    for entry in variant['omim_entries']:
        # Many prot changes in omim entries:
        changes += entry.get('prot_changes', [])

    return sorted({c for c in changes if c})
