def infer_risk_alleles(variant):
    """
    Infer the risk alleles from the genomic alleles with entries in ClinVar,
    ClinVar Variations, SnpEff, VEP, or GWAS Catalog.
    """
    risk_alleles = set()

    sources = [
        'all_clinvar_variations',
        'all_clinvar_entries',
        'all_gwas_associations',
        'all_snpeff_annotations',
        'all_vep_annotations',
    ]

    for source in sources:
        entries = variant.fillna(False)[source] or []
        for entry in entries:
            risk_alleles.add(entry.get('genomic_allele'))

    # GWAS associations and VEP annotations might add some None values
    return sorted(allele for allele in risk_alleles if allele)
