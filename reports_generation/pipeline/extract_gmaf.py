def extract_gmaf(ensembl_annotation):
    """
    Given Ensembl annotation for an rs ID, extract the MAF.
    """
    return ensembl_annotation.get('MAF')
