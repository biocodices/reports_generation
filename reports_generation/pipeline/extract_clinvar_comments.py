def extract_clinvar_comments(clinvar_variations):
    """
    Given a list of ClinVar variations, extract all comments to a flat list.
    """
    comments = []
    for variation in clinvar_variations:
        for assertion in variation.get('clinical_assertions', []):
            # We could filter by "clinical_significances" here.
            comments_here = \
                assertion['clinical_significance_detail'].get('comments', [])
            for comment in comments_here:
                source = comment.get('data_source')
                type_ = comment.get('type')
                text = comment.get('text')
                comments.append(f"[{source} | {type_}] {text}")
    return comments
