import itertools


def extract_omim_phenotype_names(omim_entries):
    """Given a list of OMIM entries, return the phenotypes mentioned."""
    names = set()
    for entry in omim_entries:
        for phenotype in entry.get('phenotypes') or []:
            if not phenotype.get('name'):
                continue

            parsed_name = phenotype['name'].lower()
            parsed_name = parsed_name.replace(
                'quantitative trait locus', 'qtl'
            )
            if parsed_name not in [n.lower() for n in names]:
                names.add(phenotype['name'])

    names = names - {'not specified'}
    return sorted(name for name in names if name)


def extract_clinvar_phenotype_names(clinvar_entries):
    """Given a list of ClinVar entries, return the phenotypes mentioned."""
    names = {phenotype.get('name')
             for entry in clinvar_entries
             for phenotype in (entry.get('conditions') or [])}

    names = names - {'not provided', 'not specified'}
    return sorted(name for name in names if name)


def extract_clinvar_variation_phenotype_names(clinvar_variations):
    """Given a list of CliVar variations, return the phenotypes mentioned."""
    lists_of_phenotype_names = [variation['associated_phenotypes']
                                for variation in clinvar_variations]
    unique_names = set(itertools.chain.from_iterable(lists_of_phenotype_names))
    unique_names = unique_names - {'not provided', 'not specified'}
    return sorted(unique_names)


def extract_gwas_phenotype_names(gwas_associations):
    """Given a list of GWAS Catalog associations, return the unique traits
       mentioned."""
    return sorted({association['trait'] for association in gwas_associations})


def extract_phenotype_names(variant):
    """Given a variant with 'clinvar_phenotype_names', 'gwas_pheno_names' and
       'omim_phenotype_names', return the unique names mentioned in both."""
    names = set(variant['clinvar_phenotype_names'] +
                variant['clinvar_variation_phenotype_names'] +
                variant['omim_phenotype_names'] +
                variant['gwas_phenotype_names'])
    return sorted(names)

