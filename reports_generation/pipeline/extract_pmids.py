def extract_pmids(variant):
    """Given a variant, extract the PubMed IDs from pubmed entries in OMIM
    entries and in GWAS Catalog entries. Returns a list."""
    pmids = set()

    for omim_entry in variant['omim_entries']:
        for pubmed_entry in omim_entry.get('pubmed_entries') or []:
            pmids.add(pubmed_entry.get('pmid'))

    for gwas_association in variant['all_gwas_associations']:
        for pubmed_entry in gwas_association.get('pubmed_entries') or []:
            pmids.add(pubmed_entry.get('pmid'))

    return sorted(pmid for pmid in pmids if pmid)
