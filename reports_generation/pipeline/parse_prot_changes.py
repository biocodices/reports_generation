import re


def parse_prot_changes(prot_changes):
    """
    Given a list of *prot_changes* with various formats, extract the unique
    values and parse them so they look like "p.Ala100Val".
    """
    pattern = r'(p\.\w{3}\d+\w{3})'
    parsed_changes = set()

    for change in prot_changes:
        match = re.search(pattern, change)
        parsed_change = match.group(1) if match else change
        parsed_changes.add(parsed_change)

    return list(parsed_changes)
