from reports_generation.helpers import AlleleCategory


def pick_worst_category(variant):
    """
    Given a Series or dict *variant*, the values at these keys are compared to
    get the worst category:

        - 'clinvar_worst_significance'
        - 'snpeff_worst_impact'
        - 'vep_worst_impact'
        - 'sample_gwas_associations' (The mere presence indicates "association")

    Returns an AlleleCategory instance.
    """
    categories = [
        variant['clinvar_worst_significance'],
        variant['snpeff_worst_impact'],
        variant['vep_worst_impact'],
    ]

    if variant['sample_gwas_associations']:
        categories.append('ASSOC')

    categories = {c for c in categories if c}

    if categories:
        return max(AlleleCategory(cat) for cat in categories)
    else:
        return AlleleCategory('NA')

