from copy import deepcopy

from reports_generation.helpers import is_nan


def dictionary_deep_nan_to_None(dictionary):
    """
    Convert all NaN values (e.g. numpy.float64) found in the dictionary to None.

    (Doesn't work for lists of lists so far.)

    (Doesn't check the keys, just the values, and it goes in deep.)
    """
    parsed_dictionary = deepcopy(dictionary)

    for key in parsed_dictionary.keys():
        value = parsed_dictionary[key]

        if is_nan(value):
            parsed_dictionary[key] = None
        elif type(value) == list:

            for i, item in enumerate(value):
                if is_nan(item):
                    value[i] = None
                elif type(item) == dict:
                    value[i] = dictionary_deep_nan_to_None(item)  # Recursive

                # Blindspot: This branch doesn't deal with lists, so lists
                # of lists with NaNs deep inside will fail to be fixed.

        elif type(value) == dict:
            parsed_dictionary[key] = dictionary_deep_nan_to_None(value)  # Recursive


    return parsed_dictionary
