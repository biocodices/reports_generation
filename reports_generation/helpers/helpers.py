from Bio.Seq import Seq

from vcf_to_dataframe import available_samples


def check_samples_in_vcf(samples, vcf):
    """
    Given a list of *samples*, check if the samples exist in the *vcf* and
    return the same list. Raise if any sample is not in the vcf.
    """
    vcf_available_samples = available_samples(vcf)

    if isinstance(samples, str) or isinstance(samples, int):
        samples = [samples]

    for sample in samples:
        if sample not in vcf_available_samples:
            raise ValueError('Sample {} not found in {}' .format(sample, vcf))

    return samples

def reverse_complement(nucleotide):
    """Given a DNA nucleotide, return the reverse complement."""
    return str(Seq(nucleotide).reverse_complement())

