from collections import Counter


def extract_key_of_all_dicts(list_of_dicts, key, count=False):
    """
    Given a list of dictionaries with the same *key*, extract unique values
    of that key across all the dictionaries and return it as a list.

    If *count* is set to True, returns a Counter instead:

    {
        value-1: number-of-occurrences,
        value-2: number-of-occurrences,
        ...
    }
    """
    if not list_of_dicts:
        return None

    values = [dic[key] for dic in list_of_dicts]

    if count:
        return Counter(values)
    else:
        return sorted(set(values))

