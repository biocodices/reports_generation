from functools import lru_cache


class AlleleCategory:
    _categories = [
        # Keep these tags unique
        ('PROT', 'protective', 'ClinVar'),
        ('BEN', 'Benign', 'ClinVar'),
        ('LBEN', 'Likely benign', 'ClinVar'),
        ('MODIF', 'Modifier', 'SnpEff'),
        ('LOW', 'Low impact', 'SnpEff'),
        ('MODERATE', 'Moderate impact', 'SnpEff'),
        ('ASSOC', 'association', 'ClinVar'),
        ('RISKF', 'risk factor', 'ClinVar'),

        ('NA', 'not provided', 'ClinVar'),
        ('UNK', 'Unknown', ''),
        ('AFF', 'Affects', 'ClinVar'),
        ('DRUG', 'drug response', 'ClinVar'),
        ('OTHER', 'other', 'ClinVar'),
        ('UNCERT', 'Uncertain significance', 'ClinVar'),
        ('CONFL', 'Conflicting interpretations of pathogenicity', 'ClinVar'),
        ('HIGH', 'High impact', 'SnpEff'),
        ('LPAT', 'Likely pathogenic', 'ClinVar'),
        ('PAT', 'Pathogenic', 'ClinVar'),
    ]
    categories = [(rank, tag, desc, source)
                  for rank, (tag, desc, source) in enumerate(_categories)]

    def __init__(self, name):
        """
        Initiate with either a ClinVar significance like 'Pathogenic' or
        a SnpEff impact like 'HIGH'. If the name is not found among those, it
        will try to match it to ClinVar's tags like 'PAT' or SnpEff impact
        descriptions like 'High impact'.
        """
        category = self._find_in_tags(name) or self._find_in_descriptions(name)

        if not category:
            from pprint import pprint
            available = [category[0:2] for category in self._categories]

            print('\nAllele Categories I know of:\n')
            pprint(available)
            print('\n')

            raise ValueError("I don't know the category '{}'. Choose one of"
                             "the categories listed above.".format(name))

        self.rank, self.tag, self.description, self.source = category

    def __repr__(self):
        return "{}('{}')".format(self.__class__.__name__, self.tag)

    def __str__(self):
        return self.tag

    def _find_in_tags(self, name):
        for category in self.categories:
            if name == category[1]:
                return category

    def _find_in_descriptions(self, name):
        for category in self.categories:
            if name and name.lower() == category[2].lower():
                return category

    def __lt__(self, other):
        return self.rank < other.rank

    def __eq__(self, other):
        return self.rank == other.rank

    def __le__(self, other):
        return self.rank <= other.rank

    def __hash__(self):
        return hash(self.tag)

    @classmethod
    def get_all(cls):
        return sorted([cls(cat[1]) for cat in cls.categories], reverse=True)

    @classmethod
    def categories_from_source(cls, source):
        return [cat for cat in cls.get_all() if cat.source == source]

    @classmethod
    def snpeff_categories(cls):
        return cls.categories_from_source('SnpEff')

    @classmethod
    def clinvar_categories(cls):
        return cls.categories_from_source('ClinVar')

    @classmethod
    @lru_cache()
    def get_category_rank(cls, cat):
        return cls(cat).rank

