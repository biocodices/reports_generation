from reports_generation.helpers import is_nan


def replace_nan_with_None(df):
    """
    Replaces NaN values found in the input dataframe with None whenever
    possible. NaN in float d-types will be kept by pandas, but in object
    columns they will be replaced.

    Returns a new dataframe.
    """
    return df.applymap(lambda value: None if is_nan(value) else value)
