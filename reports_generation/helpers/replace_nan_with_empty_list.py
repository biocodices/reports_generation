def _replace_nan_with_empty_list(series):
    """Given a pandas Series, replace NaN/None values with an empty list."""
    series = series.astype(object)
    null_indices = series.isnull()
    series.loc[null_indices] = [[] for _ in series.loc[null_indices]]
    return series


def replace_nan_with_empty_list(df, columns):
    """
    Fills 'entries' fields that are NaN with empty lists to avoid type issues
    downstream in the pipeline.

    Modifies the dataframe *in place*, but only for some columns.
    """
    for column in columns:
        if column in df:
            df[column] = _replace_nan_with_empty_list(df[column])

    return df
