from functools import partial
from itertools import groupby
from collections import defaultdict, OrderedDict

from reports_generation.helpers import AlleleCategory


def group_annotations_by_fields(annotations, fields):
    """
    Given a list of annotations, group them by the given *fields*. The keys
    will be the joint values of said fields.

    Returns a dictionary.
    """
    grouped = defaultdict(list)

    for annotation in annotations:
        key = key_from_fields(annotation, fields)
        grouped[key].append(annotation)

    return dict(grouped)


def key_from_fields(annotation, fields):
    """
    Given an annotation (dict or Series), make a key with the values at the
    *fields* in it (in the passed order).
    """
    values = [annotation.get(field) for field in fields]

    # Replace None with empty string because I still want that absent datum
    # to be "present":
    values = [val or "" for val in values]

    # Lists are not hashable, so join their values too:
    values = ['-'.join(maybe_list) if isinstance(maybe_list, list)
              else maybe_list for maybe_list in values]

    if not any(values):
        raise KeyError('None of these keys ({}) are present in this '
                       'annotation: {}'
                       .format(fields, annotation))

    return ':'.join(str(value) for value in values)


def sort_groups_by_worst_category(grouped_annotations):
    """
    Given a dictionary of grouped annotations like:

        {
            'feature-1': {'Likely pathogenic': [...],
                          'Benign': [..]}

            'feature-2': {'Pathogenic': [..]}
        }

    return an OrderedDict with the features sorted by the worst category found
    among the keys of their annotations. For instance, 'feature-2' in the
    example would be first in the result, since its worst category is
    'Pathogenic', whereas 'feature-1's worst category is 'Likely pathogenic'.
    """
    def find_worst_category_ranking(feature__entries_by_category):
        # Returns the ranking of the worst category found in the group
        entries_by_category = feature__entries_by_category[1]
        categories_present = [AlleleCategory(category)
                              for category in entries_by_category]
        return max(categories_present).rank

    return OrderedDict(sorted(grouped_annotations.items(),
                              key=find_worst_category_ranking,
                              reverse=True))
