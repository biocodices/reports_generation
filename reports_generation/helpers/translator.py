import re
from os.path import join, basename
from glob import glob
import yaml
import logging


logger = logging.getLogger(__name__)


class Translator:
    """
    Jinja2 needs an object that responds to #ugettext and #ngettext (the latter
    should deal with plurals).
    """
    def __init__(self, translations_dir):
        self.translations_dir = translations_dir
        self.untranslated = set()
        self.translations = self._load_translations(translations_dir)

    def ugettext(self, key):
        return self._deep_search(key, self.translations)

    def ngettext(self, key, number):
        # TODO: this should be implemented using a serious inflector
        singular_translation = self.ugettext(key)
        if number == 1:
            return singular_translation

        plural_translation = self.ugettext(key + '_plural')

        if '**' in plural_translation:
            # ^ '**' is a marker for not found translations

            if re.search(r'[aeiou]$', singular_translation):
                suffix = 's'
            elif singular_translation.endswith('s'):
                suffix = ''
            else:
                suffix = 'es'

            plural_translation = singular_translation + suffix

        return plural_translation

    def _deep_search(self, key, dictionary, level=0):
        """
        Search for the given key in the given dictionary. If the key has ':' in
        it, it will be split in two and the first part will be used as the key
        to access a nested dictionary, while the second part will be treated
        as a [possibly compound] key for the new sub-dict.

        Keys are converted to lowercase and strip()ed.

        Example:
            > self._deep_search('genes:TTN', translations_dict)
            > # => Will search in self.translations['genes']['ttn']
        """
        original_key = key
        keys = str(key).lower().strip().split(':', maxsplit=1)
        key = keys.pop(0)
        # ^ Left part of the ':' should be the key to use in the passed dict

        try:
            result = dictionary[key]
        except KeyError:
            if level == 0:
                self.untranslated.add(original_key)
                logger.warning('Untranslated: {}'.format(original_key))
                return '** {} **'.format(original_key)
            else:
                raise  # Bubble up exceptions up to level 0

        if not keys:
            return result

        # If more nested keys are available, search deeper
        # with the remaining keys in the nested dictionary

        if not isinstance(result, dict):
            msg = 'Keys left ({}) but result under {} is not a dictionary'
            raise ValueError(msg.format(keys, key))

        try:
            return self._deep_search(':'.join(keys), result, level=level+1)

        # FIXME: there must be a way to DRY up this logic:
        except KeyError:
            if level == 0:
                logger.warning('Untranslated: {}'.format(original_key))
                self.untranslated.add(original_key)
                return '** {} **'.format(original_key)
            else:
                raise  # Bubble up exceptions up to level 0

    @classmethod
    def _load_translations(cls, translations_dir):
        """
        Searches recursively for YAML files in the *translations_dir*
        directory and loads all translations in them, returning a dictionary.
        Files in the directory should have unique names, since their filenames
        (and NOT their parents dirs) will be used as a namespace. The filename
        'default.yml' will be treated specially (wherever its found): its
        keys will be loaded into the root level of the dictionary.

        Keys are lowercased for easier matching.

        For instance, the following dir structure:

            [translations_dir]
            |__ default.yml <-- will be read
            |__ [external]
                |__ [source1]
                    |__ source1.yml <-- will be read
                |__ source2.yml <-- will be read
            |__ [template]
                |__ titles.yml <-- will be read

        Will yield the translations dictionary:

            translations = {

                # 'default.yml' is read to the root level:

                <keys in default.yml>

                # the rest of the YAML files are read to their respective
                # namespaces, all direct children of root:

                source1: {
                    <keys in source1.yml>
                }
                source2: {
                    <keys in source2.yml>
                }
                titles: {
                    <keys in titles.yml>
                }

            }
        """
        found_yamls = glob(join(translations_dir, '*.yml'))
        found_yamls += glob(join(translations_dir, '**/*.yml'))

        if not found_yamls:
            msg = "Couldn't find any yaml translation files in {}"
            raise Exception(msg.format(translations_dir))

        translations = {}

        for fn in found_yamls:
            namespace = basename(fn).replace('.yml', '')
            with open(fn) as f:
                info = yaml.load(f.read())
                info = cls._lower_keys(info)

                if namespace == 'default':
                    # Add the keys in the 'default' namespace to the root level
                    translations.update(info)
                else:
                    translations[namespace] = info

        if not translations:
            msg = "No translations loaded from the files in {}"
            raise Exception(msg.format(translations_dir))

        return translations

    @staticmethod
    def _lower_keys(dictionary):
        """Given a flat dictionary, return a new dict with lowercase keys."""
        return {key.lower(): value for key, value in dictionary.items()}

