def is_SNP(variant):
    """
    Given a variant with 'vcf_info', 'vcf_ref' and 'vcf_alt' fields, determine
    if it's a SNP or not.
    """
    if 'VT' in variant['vcf_info']:
        return variant['vcf_info']['VT'] == 'SNP'

    # If no Variant Type data in the VCF, infer from REF and ALT alleles:

    single_nucleotides = ['A', 'T', 'C', 'G']
    ref_is_single = variant['vcf_ref'] in single_nucleotides
    alt_are_single = all(alt in single_nucleotides for alt in variant['vcf_alt'])

    return ref_is_single and alt_are_single

