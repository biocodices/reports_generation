"""
The purpose of this class is to aid the parsing of the HTML report
once it has already been manually edited (i.e. some variants might have
been deemed non-reportable, some data might have been deleted because
it was duplicated, etc.).

It takes an HTML report that should have '.variant-detail' elements, with all
the variant details in them. Any tag with attribute "data-key" (and optionally
"data-value") will be parsed.
"""

import re
from collections import OrderedDict

import pandas as pd
from bs4 import BeautifulSoup
from more_itertools import unique_everseen, always_iterable

from reports_generation.helpers import is_nan


class ReportParser:
    def __init__(self, report_filepath):
        """
        Initialize pointing to an HTML report.

        > parser = ReportParser('/path/to/report.html')
        """
        self.report_filepath = report_filepath
        self._read_report_file()


    def _read_report_file(self):
        with open(self.report_filepath) as f:
            self.html = f.read()


    def parse_the_report(self, include_nonreportable=False,
                         return_dataframe=True):
        """
        Parse the HTML report in *self.report_filepath* and return a dataframe
        with information per reportable variant.

        Set *include_nonreportable* to True to include all variants.
        Set *return_dataframe* to False to return a dictionary instead.
        """
        self.soup = self._make_soup(self.html)
        self.variants_html = self._extract_variants_from_soup(
            self.soup,
            include_nonreportable=include_nonreportable,
        )
        variant_records = self._parse_variants_html(self.variants_html)

        report_title = self._extract_title_from_soup(self.soup)
        for variant_record in variant_records.values():
            variant_record['report_title'] = report_title

        if return_dataframe:
            return self._info_dict_as_dataframe(variant_records)
        else:
            return variant_records

    @staticmethod
    def _make_soup(html):
        """Read self.html into self.soup."""
        return BeautifulSoup(html, 'lxml')

    def _parse_variants_html(self, html_variants):
        """
        Extract the info of all the *html_variants* and return it as a
        dictionary with key/values variant_number: variant_data.
        """
        info = OrderedDict()

        for variant_html in html_variants:
            variant_info = self._extract_variant_info(variant_html)
            id_ = variant_info['variant_rank_number']
            info[id_] = variant_info

        return info

    @staticmethod
    def _extract_title_from_soup(soup):
        return soup.select_one('.main-title').get_text().strip()

    @staticmethod
    def _extract_variants_from_soup(soup, include_nonreportable=False):
        """
        Return a list of the HTML variants from the *soup*, leaving out the
        ones with class "non-reportable".
        """
        html_variants = soup.select('.variant-detail')

        if not include_nonreportable:
            html_variants = [html_variant for html_variant in html_variants
                             if not 'non-reportable' in html_variant['class']]

        return html_variants

    @staticmethod
    def _extract_variant_info(variant_html):
        """
        Given the HTML element of a variant, extract information contained in
        elements with a "data-key" attribute (and possibly "data-value"),
        as long as they are not children of a ".non-reportable" element.

        If many elements share the same data-key, their values will be combined
        in a list.

        Returns a dictionary.
        """
        info = OrderedDict()
        html_elements_with_info = variant_html.find_all(attrs={'data-key': True})

        for html_element in html_elements_with_info:
            parent_classes = [p.get('class', '') for p in html_element.parents]
            non_reportable_parent = any('non-reportable' in klass
                                        for klass in parent_classes)

            if non_reportable_parent:
                continue

            key = html_element["data-key"]
            try:
                value = html_element["data-value"]
            except KeyError:
                value = re.sub(r'\s+', ' ', html_element.text).strip()

            if not value or is_nan(value):
                value = 'n/a'

            if key in info:
                # If there's more than one datum for this key, make it a list
                value = list(always_iterable(info[key])) + [value]

            info[key] = value

        return info

    @staticmethod
    def _info_dict_as_dataframe(info_dict):
        """
        Given an *info_dict* of variants (as keys) and their properties (as
        dictionaries in the values), return a pandas DataFrame.

        Lists values are made unique.
        """
        df = pd.DataFrame(info_dict).transpose()

        # FIXME: This field "gmaf" is hardcoded.
        # Maybe we should check for any 'float' fields?
        # Remember they all come as strings, and OMIM variant IDs like
        # 603335.1234 will be mistaken for floats in a simple check.
        if 'gmaf' in df:
            df['gmaf'] = (
                df['gmaf']
                .astype(float)
                .map(lambda f: round(f, 4), na_action='ignore')
            )

        list_fields = [col for col in df.columns
                       if any(df[col].map(type) == list)]

        for field in list_fields:
            df[field] = (df[field]
                        .map(always_iterable)
                        .map(unique_everseen)
                        .map(list)
                        .map(lambda l: None if all(str(el) == 'nan' for el in l) else l))

        # Since the input info_dict is and OrderedDict, I want to keep the
        # order of the keys in the dataframe columns. This hack *kind of*
        # achieves that:
        all_fields_in_order = []
        for dic in info_dict.values():
            for key in dic.keys():
                if key not in all_fields_in_order:
                    all_fields_in_order.append(key)

        return df[all_fields_in_order]

