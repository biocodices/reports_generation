import pandas as pd


def is_nan(value):
    """
    Check if a value is NaN or None. Iterables are treated as not NaN, no matter their
    contents.
    """
    if hasattr(value, '__iter__'):
        return False
    else:
        return pd.isnull(value)

